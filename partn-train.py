import os
from typing import Tuple, Dict, List, Union, Any, Optional
import argparse
import random
import warnings

import h5py
import yaml
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from tqdm import tqdm

from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model

import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from weaver.nn.model import ParticleNet

from modules import data, eval, exec, fix_seed, file_util

DEBUG = False

def get_coord_idx(var_trks_use:List[str]):
    return (var_trks_use.index("deta"), var_trks_use.index("dphi"))

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dir_path", type=str)

    args = parser.parse_args()
    dir_path = args.dir_path

    experiment = Experiment(
        project_name="particlenet-ftag3",
    )
    experiment.set_name(os.path.basename(dir_path))
    experiment_key = experiment.get_key()

    with open(os.path.join(dir_path, "config.yaml"), "r") as f:
        config = yaml.safe_load(f)
        
    # experiment keyをconfigに追記
    if "experiment_key" in config.keys():
        print("overwriting existing experiment_key:", config["experiment_key"])
    with open(os.path.join(dir_path, "config.yaml"), "w") as f:
            config["experiment_key"] = experiment_key
            yaml.dump(config, f)
        

    # seed settings
    seed = config["seed"]
    fix_seed.fix_seed(seed)

    experiment.log_asset(os.path.join(dir_path, "config.yaml"))
    flat_config = file_util.flatten_dict_parent_key_removed(config)
    experiment.log_parameters(flat_config)

    # device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    train_loader = data.OutMemoryDataLoader(
        dir_path = config["dataset"]["data_path"],
        preload_factor=1000000,
        var_trks_use = config["dataset"]["var_trks_use"],
        var_trks_all = config["dataset"]["var_trks_all"],
        size = config["dataset"]["train_size"],
    ).dataloader()

    val_loader = data.InMemoryDataLoader(
        is_validation=True,
        dir_path = config["dataset"]["data_path"],
        batch_size=config["dataset"]["batch_size"],
        var_trks_use = config["dataset"]["var_trks_use"],
        var_trks_all = config["dataset"]["var_trks_all"],
        size = config["dataset"]["validation_size"],
    ).dataloader()

    if DEBUG:
        for (X, y) in train_loader:
            print("train X:", X.shape, "train y:", y.shape)
            break
        for (X, y) in val_loader:
            print("val X:", X.shape, "val y:", y.shape)
            break


    # make model
    model = ParticleNet.ParticleNet(
        input_dims=len(config["dataset"]["var_trks_use"]),
        num_classes=3,
        conv_params=config["hyperparameters"]["model"]["conv_params"],
        fc_params=config["hyperparameters"]["model"]["fc_params"],
        use_fusion=True,
        use_fts_bn=True,
        use_counts=True,
        for_inference=False,
        for_segmentation=False,   
    ).to(device)

    # optimizer, scheduler, loss function
    optimizer = torch.optim.AdamW(
        model.parameters(),
        lr=config["hyperparameters"]["optimizer"]["lr_init"],
        weight_decay=config["hyperparameters"]["optimizer"]["weight_decay"],
    )

    if config["hyperparameters"]["scheduler"]["name"] == "CosineAnnealingWarmRestarts":
        scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            optimizer,
            T_0=config["hyperparameters"]["scheduler"]["T_0"],
            T_mult=config["hyperparameters"]["scheduler"]["T_mult"],
            eta_min=config["hyperparameters"]["scheduler"]["eta_min"],
        )
    elif config["hyperparameters"]["scheduler"]["name"] == "CosineDecayLR":
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
            optimizer,
            T_max=config["hyperparameters"]["scheduler"]["T_max"],
            eta_min=config["hyperparameters"]["scheduler"]["eta_min"],
        )
    else:
        raise NotImplementedError

    loss_fn = torch.nn.BCEWithLogitsLoss(reduction="none")

    print("start training")
    
    torch.autograd.set_detect_anomaly(True) if DEBUG else None
    history = []
    for t in range(config["hyperparameters"]["epochs"]):
        print(f"Epoch {t}")
        with experiment.train():
            train_loss, train_acc, train_y_test, train_y_pred = exec.train_partN_with_offline_loader(
                model = model,
                dataloader=train_loader,
                batch_size = config["dataset"]["batch_size"],
                optimizer = optimizer,
                loss_fn = loss_fn,
                scheduler = scheduler,
                scheduler_step = config["hyperparameters"]["scheduler"]["step_type"],
                device = device,
                coord_idx=get_coord_idx(config["dataset"]["var_trks_use"]),
            )
            
            train_btag_rej_c, train_btag_rej_l, train_ctag_rej_b, train_ctag_rej_l = eval.eval_train_val_executor(
                mode="training",
                dir_path=dir_path,
                config=config,
                experiment=experiment,
                y_pred=train_y_pred,
                y_test=train_y_test,
                total_loss=train_loss,
                total_acc=train_acc,
                epoch=t,
            )

        with experiment.validate():
            val_loss, val_acc, val_y_test, val_y_pred = exec.validate_partN(
                model = model,
                dataloader = val_loader,
                loss_fn = loss_fn,
                device = device,
                coord_idx=get_coord_idx(config["dataset"]["var_trks_use"]),
            )
            
            valid_btag_rej_c, valid_btag_rej_l, valid_ctag_rej_b, valid_ctag_rej_l = eval.eval_train_val_executor(
                mode="validation",
                dir_path=dir_path,
                config=config,
                experiment=experiment,
                y_pred=val_y_pred,
                y_test=val_y_test,
                total_loss=val_loss,
                total_acc=val_acc,
                epoch=t,
            )

        history.append([t, train_loss, train_acc, train_btag_rej_c, train_btag_rej_l, train_ctag_rej_b, train_ctag_rej_l, val_loss, val_acc, valid_btag_rej_c, valid_btag_rej_l, valid_ctag_rej_b, valid_ctag_rej_l])
        torch.save(model.state_dict(), os.path.join(dir_path, f"{t:03d}", "model.pth"))
        
    df_history = pd.DataFrame(
        history,
        columns=["epoch", "train_loss", "train_acc", "train_btag_rej_c", "train_btag_rej_l", "train_ctag_rej_b", "train_ctag_rej_l", "val_loss", "val_acc", "val_btag_rej_c", "val_btag_rej_l", "val_ctag_rej_b", "val_ctag_rej_l"]
    )
    df_history.to_csv(os.path.join(dir_path, "history.csv"), index=False)
    
    
    print("finish training")