import os
from typing import Tuple, Dict, List, Union, Any, Optional
import argparse
import random
import warnings

import h5py
import yaml
import json
import csv
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from tqdm import tqdm

from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model

import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from modules import data, eval, exec, fix_seed, MyParT, file_util

warnings.simplefilter("default")

DEBUG = False

def get_lorentz_idx(var_trks_use:List[str]):
    return {
        "pt": var_trks_use.index("pt"),
        "deta": var_trks_use.index("deta"),
        "dphi": var_trks_use.index("dphi"),
    }
    

if __name__ == "__main__":
    load_dotenv()
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dir_path", type=str)
    parser.add_argument("-r", "--resume", action="store_true") # store_true: このコマンドが与えられていたらTrueになる

    args = parser.parse_args()
    dir_path:str = args.dir_path
    is_resume:bool = args.resume
    
    with open(os.path.join(dir_path, "config.yaml"), "r") as f:
        config = yaml.safe_load(f)
    
    if not is_resume:
        experiment = Experiment(
            project_name="part-ftag",
        )
        experiment.set_name(os.path.basename(dir_path))
        experiment_key = experiment.get_key()
        # experiment keyをconfigに追記
        if "experiment_key" in config.keys():
            print("overwriting existing experiment_key:", config["experiment_key"])
        with open(os.path.join(dir_path, "config.yaml"), "w") as f:
                config["experiment_key"] = experiment_key
                yaml.dump(config, f)
        experiment.log_asset(os.path.join(dir_path, "config.yaml"))
        flat_config = file_util.flatten_dict_parent_key_removed(config)
        experiment.log_parameters(flat_config)
    else:
        # get last epoch
        print("trying to resume experiment...")
        if os.path.exists(os.path.join(dir_path, "history.csv")):
            last_epoch = pd.read_csv(os.path.join(dir_path, "history.csv"))["epoch"].max()
            if last_epoch > config["hyperparameters"]["epochs"]:
                raise ValueError("last epoch is larger than epochs in config.yaml")
            print("last epoch has been obtained:", last_epoch)
        else:
            raise ValueError("history.csv does not exist")
        # resume experiment
        experiment_key = config["experiment_key"]
        experiment = Experiment(
            previous_experiment=experiment_key,
            project_name="part-ftag",
        )
        experiment.set_name(os.path.basename(dir_path))
        print("resuming experiment:", experiment_key)


    is_multi_task:bool = config["is_multi_task"] if "is_multi_task" in config.keys() else False
    print("is_multi_task:", is_multi_task)

    # seed settings
    seed = config["seed"]
    fix_seed.fix_seed(seed)
    
    preload_factor = 1_000_000
    
    with open(os.path.join(config["dataset"]["data_path"], "scale_dict.json"), "r") as f:
        scale_dict = json.load(f)
    
    # device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    # dataloader preparation
    print("OutMemoryDataLoader with preload_factor", preload_factor)
    train_loader = data.OutMemoryDataLoader(
        dir_path = config["dataset"]["data_path"],
        preload_factor = preload_factor,
        var_trks_use = config["dataset"]["var_trks_use"],
        var_trks_all = config["dataset"]["var_trks_all"],
        size = config["dataset"]["train_size"],
        is_multi_task=is_multi_task,
    ).dataloader()
        
    
    val_loader = data.InMemoryDataLoader(
        is_validation=True,
        dir_path = config["dataset"]["data_path"],
        batch_size = config["hyperparameters"]["batch_size"],
        var_trks_use = config["dataset"]["var_trks_use"],
        var_trks_all = config["dataset"]["var_trks_all"],
        size = config["dataset"]["validation_size"],
        is_multi_task=is_multi_task,
    ).dataloader()
    
    # make model
    if is_multi_task:
        model = MyParT.MyParTTaggerMultiTask(
            **config["hyperparameters"]["model"],
        ).to(device)
    else:
        model = MyParT.MyParTTagger(
            **config["hyperparameters"]["model"],
        ).to(device)
    if is_resume:
        model.load_state_dict(torch.load(os.path.join(dir_path, f"{last_epoch:03d}", "model.pth")))
    else:
        experiment.set_model_graph(str(model))

    # optimizer, scheduler, loss function
    if config["hyperparameters"]["optimizer"]["name"] == "adamw":
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=config["hyperparameters"]["optimizer"]["lr_init"],
            weight_decay=config["hyperparameters"]["optimizer"]["weight_decay"],
        )
    elif config["hyperparameters"]["optimizer"]["name"] == "ranger":
        from weaver.utils.nn.optimizer import ranger # LookAhead + RAdam
        optimizer = ranger.Ranger(
            model.parameters(),
            **config["hyperparameters"]["optimizer"]["params"] if "params" in config["hyperparameters"]["optimizer"].keys() else {},
        )
    else:
        raise ValueError("optimizer must be adamw or ranger")
    
    if config["hyperparameters"]["scheduler"]["name"] == "CosineAnnealingWarmRestarts":
        scheduler = torch.optim.lr_scheduler.CosineAnnealingWarmRestarts(
            optimizer,
            T_0=config["hyperparameters"]["scheduler"]["T_0"],
            T_mult=config["hyperparameters"]["scheduler"]["T_mult"],
            eta_min=config["hyperparameters"]["scheduler"]["eta_min"],
        )
    elif config["hyperparameters"]["scheduler"]["name"] == "CosineAnnealingLR":
        scheduler = torch.optim.lr_scheduler.CosineAnnealingLR(
            optimizer,
            T_max=config["hyperparameters"]["scheduler"]["T_max"],
            eta_min=config["hyperparameters"]["scheduler"]["eta_min"],
        )
    elif config["hyperparameters"]["scheduler"]["name"] is None:
        scheduler = None
    else:
        raise ValueError("scheduler must be CosineAnnealingWarmRestarts or CosineAnnealingLR")
    
    if is_resume and scheduler is not None:
        if config["hyperparameters"]["scheduler"]["step_type"] == "epoch":
            for _ in range(last_epoch):
                scheduler.step()
        elif config["hyperparameters"]["scheduler"]["step_type"] == "iteration":
            for _ in range(last_epoch * len(train_loader)):
                scheduler.step()
        print("scheduler has been resumed")
    
    loss_fn = torch.nn.CrossEntropyLoss(reduction="mean")


    print("start training")
    with experiment.train():
        torch.autograd.set_detect_anomaly(True) if DEBUG else torch.autograd.set_detect_anomaly(False)
        history = []
        for t in range(config["hyperparameters"]["epochs"]) if not is_resume else range(last_epoch+1, config["hyperparameters"]["epochs"]):
            print(f"Epoch {t}")
            history_i = {}
            with experiment.train():
                if is_multi_task:
                    train_loss, train_acc, train_dict_task_loss, train_y_test, train_y_pred = exec.train_parT_multi_task(
                        model = model,
                        dataloader=train_loader,
                        batch_size = config["hyperparameters"]["batch_size"],
                        optimizer = optimizer,
                        loss_fn = loss_fn,
                        scheduler = scheduler,
                        device = device,
                        loss_weights=config["multi_task"]["loss_weights"],
                        lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                        edge_config=config["dataset"]["edge_config"],
                        scale_dict=scale_dict,
                        scheduler_step=config["hyperparameters"]["scheduler"]["step_type"],
                    )
                    history_i["train_loss_jet_classification"] = train_dict_task_loss["jet_classification"].mean()
                    history_i["train_loss_track_origin"] = train_dict_task_loss["track_origin"].mean()
                    history_i["train_loss_vertexing"] = train_dict_task_loss["vertexing"].mean()
                else:
                    train_loss, train_acc, train_y_test, train_y_pred = exec.train_parT(
                        model = model,
                        dataloader=train_loader,
                        batch_size = config["hyperparameters"]["batch_size"],
                        optimizer = optimizer,
                        loss_fn = loss_fn,
                        scheduler = scheduler,
                        device = device,
                        lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                        edge_config=config["dataset"]["edge_config"],
                        scale_dict=scale_dict,
                        scheduler_step=config["hyperparameters"]["scheduler"]["step_type"],
                    )
                
                history_i["train_loss"] = train_loss
                history_i["train_acc"] = train_acc

                with np.errstate(divide='ignore', invalid='ignore'):
                    train_btag_rej_c, train_btag_rej_l, train_ctag_rej_b, train_ctag_rej_l = eval.eval_train_val_executor(
                        mode="training",
                        dir_path=dir_path,
                        config=config,
                        experiment=experiment,
                        y_pred=train_y_pred,
                        y_test=train_y_test,
                        total_loss=train_loss,
                        total_acc=train_acc,
                        epoch=t,
                        is_multi_task=is_multi_task,
                        dict_task_losses=train_dict_task_loss if is_multi_task else None,
                    )
                history_i["train_btag_rej_c"] = train_btag_rej_c
                history_i["train_btag_rej_l"] = train_btag_rej_l
                history_i["train_ctag_rej_b"] = train_ctag_rej_b
                history_i["train_ctag_rej_l"] = train_ctag_rej_l
                
            with experiment.validate():
                if is_multi_task:
                    valid_loss, valid_acc, valid_dict_task_loss, valid_y_test, valid_y_pred = exec.validate_parT_multi_task(
                        model = model,
                        dataloader = val_loader,
                        loss_fn = loss_fn,
                        lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                        edge_config=config["dataset"]["edge_config"],
                        scale_dict=scale_dict,
                        device=device,
                        loss_weights=config["multi_task"]["loss_weights"],
                    )
                    history_i["val_loss_jet_classification"] = valid_dict_task_loss["jet_classification"].mean()
                    history_i["val_loss_track_origin"] = valid_dict_task_loss["track_origin"].mean()
                    history_i["val_loss_vertexing"] = valid_dict_task_loss["vertexing"].mean()
                else:
                    valid_loss, valid_acc, valid_y_test, valid_y_pred = exec.validate_parT(
                        model = model,
                        dataloader = val_loader,
                        loss_fn = loss_fn,
                        lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                        edge_config=config["dataset"]["edge_config"],
                        scale_dict=scale_dict,
                        device=device,
                    )
                
                history_i["val_loss"] = valid_loss
                history_i["val_acc"] = valid_acc

                with np.errstate(divide='ignore', invalid='ignore'):
                    valid_btag_rej_c, valid_btag_rej_l, valid_ctag_rej_b, valid_ctag_rej_l = eval.eval_train_val_executor(
                        mode="validation",
                        dir_path=dir_path,
                        config=config,
                        experiment=experiment,
                        y_pred=valid_y_pred,
                        y_test=valid_y_test,
                        total_loss=valid_loss,
                        total_acc=valid_acc,
                        epoch=t,
                        is_multi_task=is_multi_task,
                        dict_task_losses=valid_dict_task_loss if is_multi_task else None,
                    )
                history_i["val_btag_rej_c"] = valid_btag_rej_c
                history_i["val_btag_rej_l"] = valid_btag_rej_l
                history_i["val_ctag_rej_b"] = valid_ctag_rej_b
                history_i["val_ctag_rej_l"] = valid_ctag_rej_l

            torch.save(model.state_dict(), os.path.join(dir_path, f"{t:03d}", "model.pth"))

            if not os.path.exists(os.path.join(dir_path, "history.csv")):
                # headerを書き込む
                with open(os.path.join(dir_path, "history.csv"), "w") as f:
                    writer = csv.writer(f)
                    writer.writerow(list(history_i.keys()))
                print(f"{os.path.join(dir_path, 'history.csv')} has been created")
            

            with open(os.path.join(dir_path, "history.csv"), "a") as f:
                writer = csv.writer(f)
                writer.writerow(list(history_i.values()))


    print("finish training")