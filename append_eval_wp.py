import os
from typing import Tuple, Dict, List, Union, Any, Optional, Literal
import argparse
import random
import warnings
import datetime

warnings.simplefilter('ignore')

import h5py
import yaml
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from tqdm import tqdm

from comet_ml import Experiment, ExistingExperiment
from comet_ml.integration.pytorch import log_model

import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from weaver.nn.model import ParticleNet

from modules import data, eval, exec, fix_seed, file_util, MyParT

BTAG_WP = 0.7
CTAG_WP = 0.3


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dir_path", type=str)
    parser.add_argument("-m", "--modes", type=str, choices=["training", "validation", "all"], default="all")
    
    args = parser.parse_args()
    dir_path:int = args.dir_path
    modes:str = args.modes

    if modes == "all":
        list_mode = ["training", "validation"]
    else:
        list_mode = [modes]

    with open(os.path.join(dir_path, "config.yaml"), "r") as f:
        config = yaml.safe_load(f)
    
    # 今あるhistory.csvを読み込む
    history = pd.read_csv(os.path.join(dir_path, "history.csv"))
    # history_old_{timestamp}.csvとして保　存
    # timestamp = 20210901_2359 みたいな感じで保存
    timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M")
    history.to_csv(os.path.join(dir_path, f"history_old_{timestamp}.csv"), index=False)

    for epoch in range(config["hyperparameters"]["epochs"]):
        for mode in list_mode:
            print(f"epoch: {epoch}", end=" ")
            df_y_pred_y_test = pd.read_csv(os.path.join(dir_path, f"{epoch:03d}", "figure",mode, f"{mode}_y_pred_y_test.csv"))
            y_test = df_y_pred_y_test[["true_l", "true_c", "true_b"]].values
            y_pred = df_y_pred_y_test[["pred_l", "pred_c", "pred_b"]].values

            with np.errstate(divide="ignore", invalid="ignore"):
                btag_rej_c, btag_rej_l, ctag_rej_b, ctag_rej_l = eval.eval_train_val_executor(
                    dir_path=dir_path,
                    config=config,
                    experiment=None,
                    y_pred=y_pred,
                    y_test=y_test,
                    total_acc=None,
                    total_loss=None,
                    epoch=epoch,
                    optional_working_point={"b": BTAG_WP, "c": CTAG_WP},
                    mode=mode,
                )

            history.loc[epoch, f"{mode}_btag_rej_c"] = btag_rej_c
            history.loc[epoch, f"{mode}_btag_rej_l"] = btag_rej_l
            history.loc[epoch, f"{mode}_ctag_rej_b"] = ctag_rej_b
            history.loc[epoch, f"{mode}_ctag_rej_l"] = ctag_rej_l
    history["btag_wp"] = BTAG_WP
    history["ctag_wp"] = CTAG_WP
    history.to_csv(os.path.join(dir_path, "history.csv"), index=False)
    print("Done!")