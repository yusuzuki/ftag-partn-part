# ftag-partn-part
## 概要
- 信州大学高エネルギー物理学研究室の鈴木（2023年度卒）の研究に使用したコードです
- [ParticleNet](https://arxiv.org/abs/1902.08570)や[Particle Transformer（ParT）](https://arxiv.org/abs/2202.03772)を用いて、flavour-taggingの精度向上を目指しました
- 修論本体は[こちら](https://cernbox.cern.ch/s/azDc3BzgrVMIpxM)

## ワークフロー
![image](docs/workflow.png)

## 注意点
- 信大の計算サーバーで実行しているため、ファイルパスが信大のものになっています
- 若干古いSaltやUmamiを利用しているため、現行のSaltやUmamiでは動作しない可能性があります

## ファイル構成
- `modules/` : 訓練・評価・解析に使用したモジュール群
    - `MyParT.py`: Particle Transformerの実装（weaverのコードを改変）
    -  `__init__.py`
    - `compare.py` : モデル間の性能比較のプロット作成
    - `data.py` : PyTorch用のDataset, DataLoaderの作成
    - `eval.py` : モデルの評価
    - `exec.py` : モデルの訓練
    - `file_util.py` : h5ファイルの中身の確認
    - `fix_seed.py` : 乱数シードの固定
- `append_eval_wp.py` : モデル性能のcsvファイルにワーキングポイントを追加
- `check_*.ipynb` : 各種分布の確認と作成
- `compare_*.ipynb` : モデル間の性能比較のプロット作成
- `part-train.py` : ParTの訓練スクリプト
- `part-eval.py` : ParTの評価スクリプト
- `partn-train.py` : ParTの訓練スクリプト
- `partn-eval.py` : ParTの評価スクリプト
- `requirements.txt` : 依存パッケージ
