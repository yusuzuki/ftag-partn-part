from typing import List, Dict, Tuple, Literal, Optional
from tqdm import tqdm
import numpy as np
import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from . import MyParT, eval

DEBUG = False


def train_partN_with_offline_loader(
    model: torch.nn.Module,
    dataloader: DataLoader,
    batch_size: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    scheduler,
    device: torch.device,
    coord_idx: Tuple[int],  # (deta_idx, dphi_idx)
    scheduler_step: Literal["epoch", "batch"] = "batch",
):
    y_tests = []
    y_preds = []
    model.train()
    total_loss = 0
    total_correct = 0
    total = 0

    for batch_X, batch_y in tqdm(dataloader):
        # print("\n", batch_X.shape, batch_y.shape)
        for ii in range(0, batch_X.shape[0], batch_size):
            X = batch_X[ii : ii + batch_size]
            y = batch_y[ii : ii + batch_size]
            # print(ii, X.shape, y.shape)
            points = torch.stack(
                [X[:, coord_idx[0], :], X[:, coord_idx[1], :]], dim=1
            ).to(device)
            features = X.to(device)
            mask = (X[:, 0, :][:, None, :] != 0).to(device)
            y = y.to(device)
            optimizer.zero_grad()
            y_pred = model(points, features, mask)
            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()
            loss.backward()
            optimizer.step()
            scheduler.step() if scheduler_step == "batch" else None

            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(y_pred.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())

    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    scheduler.step() if scheduler_step == "epoch" else None

    return total_loss / total, total_correct / total, y_tests, y_preds


def validate_partN(
    model: torch.nn.Module,
    dataloader: DataLoader,
    loss_fn: torch.nn.Module,
    device: torch.device,
    coord_idx: Tuple[int],  # (deta_idx, dphi_idx)
):
    model.eval()
    total_loss = 0
    total_correct = 0
    y_tests = []
    y_preds = []
    total = 0
    with torch.no_grad():
        for X, y in dataloader:
            points = torch.stack(
                [X[:, coord_idx[0], :], X[:, coord_idx[1], :]], dim=1
            ).to(device)
            features = X.to(device)
            mask = (X[:, 0, :][:, None, :] != 0).to(device)
            y = y.to(device)

            y_pred = model(points, features, mask)
            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()

            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(y_pred.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())

    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    return total_loss / total, total_correct / total, y_tests, y_preds


def test_partN(
    model: torch.nn.Module,
    dataloader: DataLoader,
    loss_fn: torch.nn.Module,
    device: torch.device,
    coord_idx: Tuple[int],  # (deta_idx, dphi_idx)
):
    model.eval()
    total_loss = 0
    total_correct = 0
    each_correct = [0, 0, 0]
    each_number = [0, 0, 0]
    total = 0
    y_preds = []
    with torch.no_grad():
        for i, (X, y) in enumerate(dataloader):
            points = torch.stack(
                [X[:, coord_idx[0], :], X[:, coord_idx[1], :]], dim=1
            ).to(device)
            features = X.to(device)
            mask = (X[:, 0, :][:, None, :] != 0).to(device)
            y = y.to(device)
            y_pred = model(points, features, mask)
            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            loss = loss_fn(y_pred, y)
            loss = loss.mean()
            total_loss += loss.item()
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            for j in range(3):
                each_correct[j] += (
                    ((y_pred.argmax(dim=1) == j) & (y.argmax(dim=1) == j)).sum().item()
                )
                each_number[j] += (y.argmax(dim=1) == j).sum().item()
            total += y.shape[0]
            y_preds.append(y_pred)

    dict_score = {
        "total_loss": total_loss / len(dataloader),
        "total_acc": total_correct / total,
        "b_acc": each_correct[2] / each_number[2],
        "c_acc": each_correct[1] / each_number[1],
        "l_acc": each_correct[0] / each_number[0],
    }

    return torch.cat(y_preds, dim=0).detach().cpu().numpy(), dict_score


def train_parT_dummy(
    model: torch.nn.Module,
    dataloader: DataLoader,
    batch_size: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    scheduler,
    device: torch.device,
):
    model.train()
    total_loss = 0
    total_correct = 0
    each_correct = [0, 0, 0]
    each_number = [0, 0, 0]
    total = 0

    for i, (X, y) in tqdm(enumerate(dataloader)):
        # print(ii, X.shape, y.shape)
        mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

        # make dummy 4-vector, v, (N, 4, P)
        vec = torch.rand((X.shape[0], 4, X.shape[2]), dtype=torch.float32) * 1000
        vec = vec * mask.expand(-1, 4, -1)

        features = X.to(device)
        mask = mask.to(device)
        y = y.to(device)
        vec = vec.to(device)

        optimizer.zero_grad()
        y_pred = model(pf_x=features, pf_v=vec, pf_mask=mask)

        if torch.any(torch.isnan(y_pred.detach().cpu())):
            # save input/output tensors as h5 for debugging
            import h5py
            import datetime

            timestamp = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
            with h5py.File(f"nan_debug_{timestamp}.h5", "w") as f:
                f.create_dataset("X", data=X.detach().cpu().numpy())
                f.create_dataset("y", data=y.detach().cpu().numpy())
                f.create_dataset("mask", data=mask.detach().cpu().numpy())
                f.create_dataset("vec", data=vec.detach().cpu().numpy())
                f.create_dataset("y_pred", data=y_pred.detach().cpu().numpy())
            raise ValueError("NaN detected in output Tensor!")

        loss = loss_fn(y_pred, y)
        total_loss += loss.sum().item()
        loss = loss.mean()
        loss.backward()
        optimizer.step()
        scheduler.step()
        total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
        for j in range(3):
            each_correct[j] += (
                ((y_pred.argmax(dim=1) == j) & (y.argmax(dim=1) == j)).sum().item()
            )
            each_number[j] += (y.argmax(dim=1) == j).sum().item()
        total += y.shape[0]

    return (
        total_loss / total,
        total_correct / total,
        each_correct[2] / each_number[2],
        each_correct[1] / each_number[1],
        each_correct[0] / each_number[0],
    )


def train_parT(
    model: torch.nn.Module,
    dataloader: DataLoader,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    batch_size: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    scheduler,
    device: torch.device,
    scheduler_step: Literal["epoch", "batch"] = "batch",
    scale_dict: Optional[dict] = None,  # from configs
    use_mask: bool = False,
    use_iteration: bool = False,
    iteration: Optional[int] = None,
    is_multi_task: bool = False,
):
    """
    Note: Only offline loader is supported.
    """
    model.train()
    total_loss = 0
    total_correct = 0
    total = 0
    y_preds = []
    y_tests = []

    for batch in tqdm(dataloader):
        if is_multi_task:
            batch_X, batch_y, batch_y_node, batch_y_edge = batch
        else:
            if use_mask:
                batch_X, batch_y, batch_mask = batch
            else:
                batch_X, batch_y = batch
        # print("\n", batch_X.shape, batch_y.shape)
        for ii in range(0, batch_X.shape[0], batch_size):
            if use_iteration:
                iteration += 1
            X: torch.Tensor = batch_X[ii : ii + batch_size]  # (N, F, P)
            y: torch.Tensor = batch_y[ii : ii + batch_size]  # (N, 3)
            # print(ii, X.shape, y.shape)
            if use_mask:
                mask: torch.Tensor = ~batch_mask[ii : ii + batch_size][
                    :, None, :
                ]  # (N, 1, P)
            else:
                mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)
            if is_multi_task:
                y_node: torch.Tensor = batch_y_node[ii : ii + batch_size]
                y_edge: torch.Tensor = batch_y_edge[ii : ii + batch_size]
                task_labels = {
                    "truthOriginLabels": y_node.to(device),
                    "truthVertexIndex": y_edge.to(device),
                }

            uu, uu_idx = None, None
            if edge_config["use_edge"]:
                uu, uu_idx = MyParT.make_tensor_pair_features(
                    x=X,
                    valid=mask,
                    lorentz_idx=lorentz_idx,
                    edge_config=edge_config,
                    scale_dict=scale_dict,
                )

            if DEBUG:
                print(uu.shape, uu_idx.shape, X.shape, y.shape, mask.shape)

                if uu.max() == torch.inf or uu.min() == -torch.inf:
                    raise ValueError("Inf detected in input Tensor!")

                if (
                    torch.any(torch.isnan(uu))
                    or torch.any(torch.isnan(uu_idx))
                    or torch.any(torch.isnan(X))
                    or torch.any(torch.isnan(y))
                    or torch.any(torch.isnan(mask))
                ):
                    raise ValueError("NaN detected in input Tensor!")

            features = X.to(device)
            mask = mask.to(device)
            uu = uu.to(device) if uu is not None else None  # (N, F, P*P)
            uu_idx = uu_idx.to(device) if uu_idx is not None else None  # (N, 2, P*P)
            y = y.to(device)

            optimizer.zero_grad()
            if is_multi_task:
                y_pred, task_preds, task_loss = model(
                    features, mask, uu, uu_idx, task_labels
                )
            else:
                y_pred = model(features, mask, uu, uu_idx)

            if torch.any(torch.isnan(y_pred.detach().cpu())):
                raise ValueError("NaN detected in output Tensor!")

            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()
            loss.backward()
            optimizer.step()
            scheduler.step() if scheduler_step == "batch" else None

            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(y_pred.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())

    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    scheduler.step() if scheduler_step == "epoch" else None

    if not use_iteration:
        return total_loss / total, total_correct / total, y_tests, y_preds
    else:
        return iteration, total_loss / total, total_correct / total, y_tests, y_preds


def train_parT_multi_task(
    model: torch.nn.Module,
    dataloader: DataLoader,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    batch_size: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    scheduler,
    device: torch.device,
    loss_weights: Dict[str, float],
    scheduler_step: Literal["epoch", "batch"] = "batch",
    scale_dict: Optional[dict] = None,  # from configs
    use_iteration: bool = False,
    iteration: Optional[int] = None,
):
    model.train()
    y_preds = []
    y_tests = []
    total_correct = 0
    total = 0
    loss_weights["all"] = 1.0
    dict_list_task_losses: Dict[str, List[float]] = {k: [] for k in loss_weights.keys()}
    # dict_list_task_preds:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]} # too large to save
    # dict_list_task_labels:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]} # too large to save

    for batch in tqdm(dataloader):
        batch_X, batch_y, batch_y_node, batch_y_edge = batch
        # print("\n", batch_X.shape, batch_y.shape)
        for ii in range(0, batch_X.shape[0], batch_size):
            if use_iteration:
                iteration += 1
            X: torch.Tensor = batch_X[ii : ii + batch_size]  # (N, F, P)
            y: torch.Tensor = batch_y[ii : ii + batch_size]  # (N, 3)
            y_node: torch.Tensor = batch_y_node[ii : ii + batch_size]
            y_edge: torch.Tensor = batch_y_edge[ii : ii + batch_size]
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)
            task_labels = {
                "track_origin": y_node.to(device),
                "vertexing": y_edge.to(device),
            }

            uu, uu_idx = None, None
            if edge_config["use_edge"]:
                uu, uu_idx = MyParT.make_tensor_pair_features(
                    x=X,
                    valid=mask,
                    lorentz_idx=lorentz_idx,
                    edge_config=edge_config,
                    scale_dict=scale_dict,
                )

            if DEBUG:
                print(uu.shape, uu_idx.shape, X.shape, y.shape, mask.shape)

                if uu.max() == torch.inf or uu.min() == -torch.inf:
                    raise ValueError("Inf detected in input Tensor!")

                if (
                    torch.any(torch.isnan(uu))
                    or torch.any(torch.isnan(uu_idx))
                    or torch.any(torch.isnan(X))
                    or torch.any(torch.isnan(y))
                    or torch.any(torch.isnan(mask))
                ):
                    raise ValueError("NaN detected in input Tensor!")

            X = X.to(device)
            mask = mask.to(device)
            uu = uu.to(device) if uu is not None else None  # (N, F, P*P)
            uu_idx = uu_idx.to(device) if uu_idx is not None else None  # (N, 2, P*P)
            y = y.to(device)

            output, dict_task_preds, dict_task_losses = model(
                pf_x=X,
                pf_mask=mask,
                pf_uu=uu,
                pf_uu_idx=uu_idx,
                task_labels=task_labels,
            )
            dict_task_losses: Dict[str, torch.Tensor] = dict_task_losses
            # dict_task_preds:Dict[str, torch.Tensor] = dict_task_preds
            # dict_task_labels:Dict[str, torch.Tensor] = task_labels | {"jet_classification": y} # merge dicts

            if torch.any(torch.isnan(output.detach().cpu())):
                raise ValueError("NaN detected in output Tensor!")

            dict_task_losses["jet_classification"] = loss_fn(output, y)
            # print([dict_task_loss[k].item() for k in dict_task_loss.keys()])
            all_loss: torch.Tensor = sum(
                [dict_task_losses[k] * loss_weights[k] for k in dict_task_losses.keys()]
            )
            all_loss.backward()
            optimizer.step()
            optimizer.zero_grad()

            dict_task_losses["all"] = all_loss

            y_pred = torch.nn.functional.softmax(output, dim=1)
            # dict_task_preds["jet_classification"] = y_pred
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(y_pred.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())
            for k in dict_task_losses.keys():
                dict_list_task_losses[k].append(
                    dict_task_losses[k].item() * loss_weights[k]
                )
            for k in ["jet_classification", "track_origin", "vertexing"]:
                # dict_list_task_preds[k].append(dict_task_preds[k].detach().cpu().numpy())
                # dict_list_task_labels[k].append(dict_task_labels[k].detach().cpu().numpy())
                pass
    scheduler.step() if scheduler_step == "epoch" else None

    concat_dict_task_losses = {
        k: np.array(dict_list_task_losses[k]) for k in dict_list_task_losses.keys()
    }
    # concat_dict_task_preds = {k: np.concatenate(dict_list_task_preds[k], axis=0) for k in dict_list_task_preds.keys()}
    # concat_dict_task_labels = {k: np.concatenate(dict_list_task_labels[k], axis=0) for k in dict_list_task_labels.keys()}

    total_loss: float = concat_dict_task_losses["all"].mean()
    total_acc: float = total_correct / total
    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    if use_iteration:
        return (
            iteration,
            total_loss,
            total_acc,
            concat_dict_task_losses,
            y_tests,
            y_preds,
        )
    else:
        return total_loss, total_acc, concat_dict_task_losses, y_tests, y_preds


def train_parT_with_offline_loader_dummy(
    model: torch.nn.Module,
    dataloader: DataLoader,
    batch_size: int,
    optimizer: torch.optim.Optimizer,
    loss_fn: torch.nn.Module,
    scheduler,
    device: torch.device,
):
    model.train()
    total_loss = 0
    total_correct = 0
    each_correct = [0, 0, 0]
    each_number = [0, 0, 0]
    total = 0

    for batch_X, batch_y in tqdm(dataloader):
        # print("\n", batch_X.shape, batch_y.shape)
        for ii in range(0, batch_X.shape[0], batch_size):
            X: torch.Tensor = batch_X[ii : ii + batch_size]  # (N, F, P)
            y: torch.Tensor = batch_y[ii : ii + batch_size]  # (N, 3)
            # print(ii, X.shape, y.shape)
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

            # make dummy 4-vector, v, (N, 4, P)
            vec = torch.rand((X.shape[0], 4, X.shape[2]), dtype=torch.float32) * 1000
            vec = vec * mask.expand(-1, 4, -1)

            features = X.to(device)
            mask = mask.to(device)
            y = y.to(device)
            vec = vec.to(device)

            optimizer.zero_grad()
            y_pred = model(pf_x=features, pf_v=vec, pf_mask=mask)

            if torch.any(torch.isnan(y_pred.detach().cpu())):
                raise ValueError("NaN detected in output Tensor!")

            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()
            loss.backward()
            optimizer.step()
            scheduler.step()
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            for j in range(3):
                each_correct[j] += (
                    ((y_pred.argmax(dim=1) == j) & (y.argmax(dim=1) == j)).sum().item()
                )
                each_number[j] += (y.argmax(dim=1) == j).sum().item()
            total += y.shape[0]

    return (
        total_loss / total,
        total_correct / total,
        each_correct[2] / each_number[2],
        each_correct[1] / each_number[1],
        each_correct[0] / each_number[0],
    )


def validate_parT(
    model: torch.nn.Module,
    dataloader: DataLoader,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    loss_fn: torch.nn.Module,
    device: torch.device,
    scale_dict: Optional[dict] = None,  # from configs
):
    model.eval()
    total_loss = 0
    total_correct = 0
    total = 0
    y_preds = []
    y_tests = []
    with torch.no_grad():
        for X, y in dataloader:
            # print(ii, X.shape, y.shape)
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

            uu, uu_idx = MyParT.make_tensor_pair_features(
                x=X,
                valid=mask,
                lorentz_idx=lorentz_idx,
                edge_config=edge_config,
                scale_dict=scale_dict,
            )

            features = X.to(device)
            mask = mask.to(device)
            uu = uu.to(device)  # (N, F, P*P)
            uu_idx = uu_idx.to(device)  # (N, 2, P*P)
            y = y.to(device)

            y_pred = model(features, mask, uu, uu_idx)
            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()
            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(y_pred.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())

    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    return total_loss / total, total_correct / total, y_tests, y_preds


def validate_parT_multi_task(
    model: torch.nn.Module,
    dataloader: DataLoader,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    loss_fn: torch.nn.Module,
    device: torch.device,
    loss_weights: Dict[str, float],
    scale_dict: Optional[dict] = None,  # from configs
):
    model.eval()
    total_loss = 0
    total_correct = 0
    total = 0
    y_preds = []
    y_tests = []
    loss_weights["all"] = 1.0
    dict_list_task_losses: Dict[str, List[float]] = {k: [] for k in loss_weights.keys()}
    # dict_list_task_preds:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]} # too large to save
    # dict_list_task_labels:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]} # too large to save
    with torch.no_grad():
        for X, y, y_node, y_edge in dataloader:
            # print(ii, X.shape, y.shape)
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

            uu, uu_idx = MyParT.make_tensor_pair_features(
                x=X,
                valid=mask,
                lorentz_idx=lorentz_idx,
                edge_config=edge_config,
                scale_dict=scale_dict,
            )

            features = X.to(device)
            mask = mask.to(device)
            uu = uu.to(device)  # (N, F, P*P)
            uu_idx = uu_idx.to(device)  # (N, 2, P*P)
            y = y.to(device)
            task_labels = {
                "track_origin": y_node.to(device),
                "vertexing": y_edge.to(device),
            }

            output, dict_task_preds, dict_task_losses = model(
                pf_x=features,
                pf_mask=mask,
                pf_uu=uu,
                pf_uu_idx=uu_idx,
                task_labels=task_labels,
            )
            dict_task_losses: Dict[str, torch.Tensor] = dict_task_losses
            # dict_task_preds:Dict[str, torch.Tensor] = dict_task_preds
            # dict_task_labels:Dict[str, torch.Tensor] = task_labels | {"jet_classification": y} # merge dicts

            if torch.any(torch.isnan(output.detach().cpu())):
                raise ValueError("NaN detected in output Tensor!")

            dict_task_losses["jet_classification"] = loss_fn(output, y)
            # print([dict_task_loss[k].item() for k in dict_task_loss.keys()])
            all_loss: torch.Tensor = sum(
                [dict_task_losses[k] * loss_weights[k] for k in dict_task_losses.keys()]
            )
            dict_task_losses["all"] = all_loss

            # apply softmax
            output = torch.nn.functional.softmax(output, dim=1)
            total_correct += (output.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            total += y.shape[0]

            y_preds.append(output.detach().cpu().numpy())
            y_tests.append(y.detach().cpu().numpy())

            for k in dict_task_losses.keys():
                dict_list_task_losses[k].append(
                    dict_task_losses[k].item() * loss_weights[k]
                )
            for k in ["jet_classification", "track_origin", "vertexing"]:
                # dict_list_task_preds[k].append(dict_task_preds[k].detach().cpu().numpy())
                # dict_list_task_labels[k].append(dict_task_labels[k].detach().cpu().numpy())
                pass

    concat_dict_task_losses = {
        k: np.array(dict_list_task_losses[k]) for k in dict_list_task_losses.keys()
    }
    # concat_dict_task_preds = {k: np.concatenate(dict_list_task_preds[k], axis=0) for k in dict_list_task_preds.keys()}
    # concat_dict_task_labels = {k: np.concatenate(dict_list_task_labels[k], axis=0) for k in dict_list_task_labels.keys()}

    y_preds = np.concatenate(y_preds, axis=0)
    y_tests = np.concatenate(y_tests, axis=0)

    total_loss: float = concat_dict_task_losses["all"].mean()
    total_acc: float = total_correct / total

    return total_loss, total_acc, concat_dict_task_losses, y_tests, y_preds


def validate_parT_dummy(
    model: torch.nn.Module,
    dataloader: DataLoader,
    loss_fn: torch.nn.Module,
    device: torch.device,
):
    model.eval()
    total_loss = 0
    total_correct = 0
    each_correct = [0, 0, 0]
    each_number = [0, 0, 0]
    total = 0
    with torch.no_grad():
        for X, y in dataloader:
            # print(ii, X.shape, y.shape)
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

            # make dummy 4-vector, v, (N, 4, P)
            vec = torch.rand((X.shape[0], 4, X.shape[2]), dtype=torch.float32) * 1000
            vec = vec * mask.expand(-1, 4, -1)

            features = X.to(device)
            mask = mask.to(device)
            y = y.to(device)
            vec = vec.to(device)

            y_pred = model(pf_x=features, pf_v=vec, pf_mask=mask)
            loss = loss_fn(y_pred, y)
            total_loss += loss.sum().item()
            loss = loss.mean()
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            for j in range(3):
                each_correct[j] += (
                    ((y_pred.argmax(dim=1) == j) & (y.argmax(dim=1) == j)).sum().item()
                )
                each_number[j] += (y.argmax(dim=1) == j).sum().item()
            total += y.shape[0]
        return (
            total_loss / total,
            total_correct / total,
            each_correct[2] / each_number[2],
            each_correct[1] / each_number[1],
            each_correct[0] / each_number[0],
        )


def test_parT(
    model: torch.nn.Module,
    dataloader: DataLoader,
    loss_fn: torch.nn.Module,
    device: torch.device,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    scale_dict: Optional[dict] = None,  # from configs
) -> Tuple[np.ndarray, Dict[str, float]]:
    model.eval()
    total_loss = 0
    total_correct = 0
    each_correct = [0, 0, 0]
    each_number = [0, 0, 0]
    total = 0
    y_preds = []
    with torch.no_grad():
        print("start inference...")
        for X, y in tqdm(dataloader):
            mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)
            uu, uu_idx = MyParT.make_tensor_pair_features(
                x=X,
                valid=mask,
                lorentz_idx=lorentz_idx,
                edge_config=edge_config,
                scale_dict=scale_dict,
            )

            features = X.to(device)
            mask = mask.to(device)
            uu = uu.to(device)  # (N, F, P*P)
            uu_idx = uu_idx.to(device)  # (N, 2, P*P)
            y = y.to(device)

            y_pred = model(features, mask, uu, uu_idx)
            # apply softmax
            y_pred = torch.nn.functional.softmax(y_pred, dim=1)
            loss = loss_fn(y_pred, y)
            loss = loss.mean()
            total_loss += loss.item()
            total_correct += (y_pred.argmax(dim=1) == y.argmax(dim=1)).sum().item()
            for j in range(3):
                each_correct[j] += (
                    ((y_pred.argmax(dim=1) == j) & (y.argmax(dim=1) == j)).sum().item()
                )
                each_number[j] += (y.argmax(dim=1) == j).sum().item()
            total += y.shape[0]
            y_preds.append(y_pred)

    dict_score = {
        "total_loss": total_loss / len(dataloader),
        "total_acc": total_correct / total,
        "b_acc": each_correct[2] / each_number[2],
        "c_acc": each_correct[1] / each_number[1],
        "l_acc": each_correct[0] / each_number[0],
    }

    return torch.cat(y_preds, dim=0).detach().cpu().numpy(), dict_score


def test_parT_multi_task(
    model: torch.nn.Module,
    dataloader: DataLoader,
    loss_fn: torch.nn.Module,
    device: torch.device,
    lorentz_idx: Dict[str, int],  # example: {"pt": 0, "eta": 1, "phi": 2, "mass": None}
    edge_config: Dict[str, bool],  # example: {"is_scale": True, "is_log": False}
    loss_weights: Dict[str, float],
    multi_task_save_h5_path: str,
    scale_dict: Optional[dict] = None,  # from configs,
) -> Tuple[np.ndarray, Dict[str, float], Dict[str, float]]:
    import h5py

    model.eval()
    total_loss = 0
    total_correct = 0
    total = 0
    y_preds = []
    loss_weights["all"] = 1.0
    dict_list_task_losses: Dict[str, List[float]] = {k: [] for k in loss_weights.keys()}
    # dict_list_task_preds:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]}
    # dict_list_task_labels:Dict[str, List[np.ndarray]] = {k: [] for k in ["jet_classification", "track_origin", "vertexing"]}
    with torch.no_grad():
        print("start inference...")
        with h5py.File(multi_task_save_h5_path, "w") as f:
            f.create_group("tests")
            f.create_group("preds")
            f.create_group("losses")
            for i, (X, y, y_node, y_edge) in enumerate(tqdm(dataloader)):
                # print(i, X.shape, y.shape, y_node.shape, y_edge.shape)
                mask = X[:, 0, :][:, None, :] != 0  # (N, 1, P)

                uu, uu_idx = MyParT.make_tensor_pair_features(
                    x=X,
                    valid=mask,
                    lorentz_idx=lorentz_idx,
                    edge_config=edge_config,
                    scale_dict=scale_dict,
                )

                features = X.to(device)
                mask = mask.to(device)
                uu = uu.to(device)  # (N, F, P*P)
                uu_idx = uu_idx.to(device)  # (N, 2, P*P)
                y = y.to(device)
                task_labels = {
                    "track_origin": y_node.to(device),
                    "vertexing": y_edge.to(device),
                }

                output, dict_task_preds, dict_task_losses = model(
                    pf_x=features,
                    pf_mask=mask,
                    pf_uu=uu,
                    pf_uu_idx=uu_idx,
                    task_labels=task_labels,
                )
                dict_task_losses: Dict[str, torch.Tensor] = dict_task_losses
                # dict_task_preds:Dict[str, torch.Tensor] = dict_task_preds
                # dict_task_labels:Dict[str, torch.Tensor] = task_labels | {"jet_classification": y} # merge dicts

                # if torch.any(torch.isnan(output.detach().cpu())):
                #     raise ValueError("NaN detected in output Tensor!")

                dict_task_losses["jet_classification"] = loss_fn(output, y)
                # print([dict_task_loss[k].item() for k in dict_task_loss.keys()])
                all_loss: torch.Tensor = sum(
                    [
                        dict_task_losses[k] * loss_weights[k]
                        for k in dict_task_losses.keys()
                    ]
                )
                dict_task_losses["all"] = all_loss

                # apply softmax
                output = torch.nn.functional.softmax(output, dim=1)
                total_correct += (output.argmax(dim=1) == y.argmax(dim=1)).sum().item()
                total += y.shape[0]

                y_preds.append(output.detach().cpu().numpy())

                for k in dict_task_losses.keys():
                    dict_list_task_losses[k].append(
                        dict_task_losses[k].item() * loss_weights[k]
                    )
                for k in ["jet_classification", "track_origin", "vertexing"]:
                    # dict_list_task_preds[k].append(dict_task_preds[k].detach().cpu().numpy())
                    # dict_list_task_labels[k].append(dict_task_labels[k].detach().cpu().numpy())
                    pass

                f["tests"].create_dataset(
                    f"track_origin_{i:06d}", data=y_node.detach().cpu().numpy()
                )
                f["tests"].create_dataset(
                    f"vertexing_{i:06d}", data=y_edge.detach().cpu().numpy()
                )
                f["tests"].create_dataset(
                    f"jet_classification_{i:06d}", data=y.detach().cpu().numpy()
                )
                f["preds"].create_dataset(
                    f"track_origin_{i:06d}",
                    data=dict_task_preds["track_origin"].detach().cpu().numpy(),
                )
                f["preds"].create_dataset(
                    f"vertexing_{i:06d}",
                    data=dict_task_preds["vertexing"].detach().cpu().numpy(),
                )
                f["preds"].create_dataset(
                    f"jet_classification_{i:06d}", data=output.detach().cpu().numpy()
                )
                f["losses"].create_dataset(
                    f"track_origin_{i:06d}",
                    data=dict_task_losses["track_origin"].detach().cpu().numpy(),
                )
                f["losses"].create_dataset(
                    f"vertexing_{i:06d}",
                    data=dict_task_losses["vertexing"].detach().cpu().numpy(),
                )
                f["losses"].create_dataset(
                    f"jet_classification_{i:06d}",
                    data=dict_task_losses["jet_classification"].detach().cpu().numpy(),
                )
                f["losses"].create_dataset(
                    f"all_{i:06d}", data=dict_task_losses["all"].detach().cpu().numpy()
                )

    concat_dict_task_losses = {
        k: np.array(dict_list_task_losses[k]).mean()
        for k in dict_list_task_losses.keys()
    }
    # concat_dict_task_preds = {k: np.concatenate(dict_list_task_preds[k], axis=0) for k in dict_list_task_preds.keys()}
    # concat_dict_task_labels = {k: np.concatenate(dict_list_task_labels[k], axis=0) for k in dict_list_task_labels.keys()}
    y_preds = np.concatenate(y_preds, axis=0)

    return y_preds, concat_dict_task_losses
