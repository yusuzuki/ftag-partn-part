import os
from typing import List, Tuple, Union, Dict, Any, Optional, Literal

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
import seaborn as sns
from comet_ml import Experiment


def calc_metrics(y_test: np.ndarray, y_pred: np.ndarray) -> Tuple[Dict[str, float]]:
    y_test_internal = y_test.argmax(1)
    _y_pred = y_pred[~((y_pred == 0).sum(1)).astype(bool)]  # 発散しているものを除く
    label = y_test_internal[~((y_pred == 0).sum(1)).astype(bool)]  # 発散しているものを除く
    y_pred_btag = _y_pred[label == 2]  # b-jet
    y_pred_ctag = _y_pred[label == 1]  # c-jet
    y_pred_ltag = _y_pred[label == 0]  # l-jet

    return {"b": y_pred_btag, "c": y_pred_ctag, "l": y_pred_ltag}, {
        "b": y_pred_btag[:, 2],
        "c": y_pred_ctag[:, 1],
        "l": y_pred_ltag[:, 0],
    }


def plot_prob_btag(
    y_pred: np.ndarray, y_test: np.ndarray, sample_name: str, save_path=None
) -> None:
    dict_label_number = {"b": 2, "c": 1, "l": 0}
    dict_label_color = {"b": "#ff7f0e", "c": "#1f77b4", "l": "#2ca02c"}

    y_pred_dict, _ = calc_metrics(y_test, y_pred)
    for i, (k, v) in enumerate(y_pred_dict.items()):
        for j, (k2, v2) in enumerate(y_pred_dict.items()):
            if i == j:
                continue

            fig, ax = plt.subplots(figsize=(8, 6))
            nbins = 100
            ax.hist(
                v[:, dict_label_number[k]],
                bins=np.linspace(0.0, 1.0, nbins),
                histtype="step",
                label=f"{k}-jets ParticleNet",
                color=dict_label_color[k],
                density=True,
            )
            ax.hist(
                v2[:, dict_label_number[k]],
                bins=np.linspace(0.0, 1.0, nbins),
                histtype="step",
                label=f"{k2}-jets ParticleNet",
                color=dict_label_color[k2],
                density=True,
            )
            ax.set(
                xlabel=f"{k}-jets probability",
                ylabel="normalized number of jets",
                ylim=[1e-2, 1e3],
                yscale="log",
                # title = f"{k} vs {k2}",
            )
            ax.legend(loc="upper right")
            # LHC-ATLAS experiment text
            anchor_x = 0.05
            anchor_y = 0.9
            ax.text(
                anchor_x,
                anchor_y,
                "ATLAS",
                fontsize=15,
                weight="bold",
                fontstyle="italic",
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            ax.text(
                anchor_x + 0.12,
                anchor_y,
                "Simulation Preliminary",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            ax.text(
                anchor_x,
                anchor_y - 0.08,
                "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            ax.text(
                anchor_x,
                anchor_y - 0.16,
                f"{sample_name} testing sample",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            if save_path is not None:
                plt.savefig(f"{save_path}/{k}_vs_{k2}.pdf", bbox_inches="tight")
                plt.savefig(f"{save_path}/{k}_vs_{k2}.png", bbox_inches="tight")
                # plt.savefig(f"{save_path}/{k}_vs_{k2}.svg", bbox_inches="tight")
            plt.show()

    fig, ax = plt.subplots(figsize=(8, 6))
    nbins = 100
    ax.hist(
        y_pred_dict["b"][:, 2],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"b-jets",
        color=dict_label_color["b"],
        density=True,
    )
    ax.hist(
        y_pred_dict["c"][:, 2],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"c-jets",
        color=dict_label_color["c"],
        density=True,
    )
    ax.hist(
        y_pred_dict["l"][:, 2],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"l-jets",
        color=dict_label_color["l"],
        density=True,
    )
    ax.set(
        xlabel=f"b-jets probability",
        ylabel="normalized number of jets",
        ylim=[1e-2, 1e3],
        yscale="log",
        # title = f"{k} vs {k2}",
    )
    ax.legend(loc="upper right")
    # LHC-ATLAS experiment text
    anchor_x = 0.05
    anchor_y = 0.9
    ax.text(
        anchor_x,
        anchor_y,
        "ATLAS",
        fontsize=15,
        weight="bold",
        fontstyle="italic",
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x + 0.12,
        anchor_y,
        "Simulation Preliminary",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x,
        anchor_y - 0.08,
        "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x,
        anchor_y - 0.16,
        f"{sample_name} testing sample",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    if save_path is not None:
        plt.savefig(f"{save_path}/btag_prob.pdf", bbox_inches="tight")
        plt.savefig(f"{save_path}/btag_prob.png", bbox_inches="tight")
        # plt.savefig(f"{save_path}/btag_prob.svg", bbox_inches="tight")
    plt.show()
    plt.close()


def plot_prob_ctag(
    y_pred: np.ndarray, y_test: np.ndarray, sample_name: str, save_path=None
) -> None:
    # dict_label_number = {"b":2, "c":1, "l":0}
    dict_label_color = {"b": "#1f77b4", "c": "#ff7f0e", "l": "#2ca02c"}

    y_pred_dict, _ = calc_metrics(y_test, y_pred)

    fig, ax = plt.subplots(figsize=(8, 6))
    nbins = 100
    ax.hist(
        y_pred_dict["c"][:, 1],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"c-jets",
        color=dict_label_color["c"],
        density=True,
    )
    ax.hist(
        y_pred_dict["b"][:, 1],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"b-jets",
        color=dict_label_color["b"],
        density=True,
    )
    ax.hist(
        y_pred_dict["l"][:, 1],
        bins=np.linspace(0.0, 1.0, nbins),
        histtype="step",
        label=f"l-jets",
        color=dict_label_color["l"],
        density=True,
    )
    ax.set(
        xlabel=f"c-jets probability",
        ylabel="normalized number of jets",
        ylim=[1e-2, 1e3],
        yscale="log",
        # title = f"{k} vs {k2}",
    )
    ax.legend(loc="upper right")
    # LHC-ATLAS experiment text
    anchor_x = 0.05
    anchor_y = 0.9
    ax.text(
        anchor_x,
        anchor_y,
        "ATLAS",
        fontsize=15,
        weight="bold",
        fontstyle="italic",
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x + 0.12,
        anchor_y,
        "Simulation Preliminary",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x,
        anchor_y - 0.08,
        "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    ax.text(
        anchor_x,
        anchor_y - 0.16,
        f"{sample_name} testing sample",
        fontsize=15,
        color="black",
        ha="left",
        va="bottom",
        transform=ax.transAxes,
    )
    if save_path is not None:
        plt.savefig(f"{save_path}/ctag_prob.pdf", bbox_inches="tight")
        plt.savefig(f"{save_path}/ctag_prob.png", bbox_inches="tight")
        # plt.savefig(f"{save_path}/ctag_prob.svg", bbox_inches="tight")
    plt.show()
    plt.close()


def plot_cm(
    y_pred: np.ndarray, y_test: np.ndarray, sample_name: str, save_path=None
) -> None:
    cm = confusion_matrix(y_test.argmax(axis=1), y_pred.argmax(axis=1))
    cm = cm.astype("float") / cm.sum(axis=1)[:, np.newaxis]
    df_cm = pd.DataFrame(cm, index=["l", "c", "b"], columns=["l", "c", "b"])

    plt.figure(figsize=(10, 7))
    sns.heatmap(df_cm, annot=True, cmap="Blues", vmin=0.0, vmax=1.0)
    plt.xlabel("Predicted")
    plt.ylabel("Truth")
    plt.xticks([0.5, 1.5, 2.5], ["l", "c", "b"])
    plt.yticks([0.5, 1.5, 2.5], ["l", "c", "b"])
    plt.title(
        "ATLAS Simulation Preliminary, $\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets "
        + f"{sample_name}"
        + " testing sample"
    )
    if save_path is not None:
        plt.savefig(f"{save_path}/cm.pdf", bbox_inches="tight")
        plt.savefig(f"{save_path}/cm.png", bbox_inches="tight")
        # plt.savefig(f"{save_path}/cm.svg", bbox_inches="tight")
    plt.show()
    plt.close()


class TagEvaluator:
    def __init__(
        self,
        y_pred: np.ndarray,
        y_test: np.ndarray,
        sample_name: str,
        f: float,
        nbins: int = 1000,
        eff_granularity: float = 0.01,
        save_path: Optional[str] = None,
        save_prefix: Optional[str] = None,
    ):
        self.y_pred = y_pred
        self.y_test = y_test
        self.sample_name = sample_name
        self.f = f
        self.nbins = nbins
        self.eff_granularity = eff_granularity
        self.save_path = save_path
        self.save_prefix = save_prefix
        self.mode = None
        self.dict_working_points = {"60%": 0.6, "70%": 0.7, "80%": 0.80, "85%": 0.85}

    def calc_metrics(self) -> Tuple[Dict[str, float]]:
        y_test_internal = self.y_test.argmax(1)
        # _y_pred = self.y_pred[np.all(self.y_pred > 1e-20, axis=1)] # 発散しているものを除く
        # label = y_test_internal[np.all(self.y_pred > 1e-20, axis=1)] # 発散しているものを除く
        _y_pred = self.y_pred.copy()
        label = y_test_internal.copy()
        y_pred_btag = _y_pred[label == 2]  # b-jet
        y_pred_ctag = _y_pred[label == 1]  # c-jet
        y_pred_ltag = _y_pred[label == 0]  # l-jet

        return {"b": y_pred_btag, "c": y_pred_ctag, "l": y_pred_ltag}, {
            "b": y_pred_btag[:, 2],
            "c": y_pred_ctag[:, 1],
            "l": y_pred_ltag[:, 0],
        }

    def discriminant(self) -> Dict[str, np.ndarray]:
        dict_metrics, _ = self.calc_metrics()

        def _D(p_tag, p_else, p_l, f):
            return np.log(p_tag / ((1 - f) * p_l + f * p_else))

        if self.mode == "b":
            D_btag = _D(
                dict_metrics["b"][:, 2],
                dict_metrics["b"][:, 1],
                dict_metrics["b"][:, 0],
                f=self.f,
            )
            D_ctag = _D(
                dict_metrics["c"][:, 2],
                dict_metrics["c"][:, 1],
                dict_metrics["c"][:, 0],
                f=self.f,
            )
            D_ltag = _D(
                dict_metrics["l"][:, 2],
                dict_metrics["l"][:, 1],
                dict_metrics["l"][:, 0],
                f=self.f,
            )
        elif self.mode == "c":
            D_btag = _D(
                dict_metrics["b"][:, 1],
                dict_metrics["b"][:, 2],
                dict_metrics["b"][:, 0],
                f=self.f,
            )
            D_ctag = _D(
                dict_metrics["c"][:, 1],
                dict_metrics["c"][:, 2],
                dict_metrics["c"][:, 0],
                f=self.f,
            )
            D_ltag = _D(
                dict_metrics["l"][:, 1],
                dict_metrics["l"][:, 2],
                dict_metrics["l"][:, 0],
                f=self.f,
            )

        # 発散(-inf, inf, nan)を除く
        D_btag = D_btag[~np.isnan(D_btag) & ~np.isinf(D_btag)]
        D_ctag = D_ctag[~np.isnan(D_ctag) & ~np.isinf(D_ctag)]
        D_ltag = D_ltag[~np.isnan(D_ltag) & ~np.isinf(D_ltag)]

        return {"b": D_btag, "c": D_ctag, "l": D_ltag}

    def calc_tag_eff(self) -> Tuple[np.ndarray, np.ndarray]:
        dict_D = self.discriminant()
        D_max = max([max(dict_D["b"]), max(dict_D["c"]), max(dict_D["l"])])
        D_min = min([min(dict_D["b"]), min(dict_D["c"]), min(dict_D["l"])])

        bins_D = np.linspace(D_min, D_max, self.nbins)

        if self.mode == "b":
            effs = [np.sum(dict_D["b"] > th) / len(dict_D["b"]) for th in bins_D]
        elif self.mode == "c":
            effs = [np.sum(dict_D["c"] > th) / len(dict_D["c"]) for th in bins_D]
        else:
            raise ValueError("mode must be b or c")

        return np.array(effs), bins_D

    def get_rej_per_eff(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        dict_D = self.discriminant()
        effs, bins_D = self.calc_tag_eff()

        D_scaled_effs = []

        for eff in np.arange(0, 1, self.eff_granularity):
            D_scaled_effs.append(bins_D[np.argmin(np.abs(effs - eff))])

        # add eff=1
        try:
            D_scaled_effs.append(bins_D[effs >= 1].max())
        except:
            D_scaled_effs.append(0.0)

        if self.mode == "b":
            rej_c, rej_l = [], []
            for th in D_scaled_effs:
                rej_c.append(1 / (np.sum(dict_D["c"] > th) / len(dict_D["c"])))
                rej_l.append(1 / (np.sum(dict_D["l"] > th) / len(dict_D["l"])))
            return np.array(rej_c), np.array(rej_l), np.array(D_scaled_effs)

        elif self.mode == "c":
            rej_b, rej_l = [], []
            for th in D_scaled_effs:
                rej_b.append(1 / (np.sum(dict_D["b"] > th) / len(dict_D["b"])))
                rej_l.append(1 / (np.sum(dict_D["l"] > th) / len(dict_D["l"])))
            return np.array(rej_b), np.array(rej_l), np.array(D_scaled_effs)

        else:
            raise ValueError("mode must be b or c")


class BTagEvaluator(TagEvaluator):
    def __init__(
        self,
        y_pred: np.ndarray,
        y_test: np.ndarray,
        sample_name: str,
        f: float,
        nbins: int = 1000,
        eff_granularity: float = 0.01,
        sample_type: Literal["training", "validation", "testing"] = "testing",
        save_path: Optional[str] = None,
        save_prefix: Optional[str] = None,
    ):
        super().__init__(
            y_pred,
            y_test,
            sample_name,
            f,
            nbins,
            eff_granularity,
            save_path,
            save_prefix,
        )
        self.mode = "b"
        self.sample_type = sample_type

    def plot_rejection(
        self,
        xlim: Tuple[float, float] = (0.2, 1),
        ylim: Tuple[float, float] = (1e0, 1e5),
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        rej_c, rej_l, _ = self.get_rej_per_eff()
        fig, ax = plt.subplots(figsize=(8, 6))
        _x = np.arange(0, 1 + self.eff_granularity, self.eff_granularity)
        ax.plot(_x, rej_c, label="c-jets rejection")
        ax.plot(_x, rej_l, label="l-jets rejection")
        ax.set(
            xlim=xlim,
            xlabel="b-jets efficiency",
            ylabel="background rejection",
            yscale="log",
            ylim=ylim,
        )
        ax.legend()
        ax.grid()

        # LHC-ATLAS experiment text
        anchor_x = 0.15
        anchor_y = 0.9
        ax.text(
            anchor_x,
            anchor_y,
            "ATLAS",
            fontsize=15,
            weight="bold",
            fontstyle="italic",
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x + 0.12,
            anchor_y,
            "Simulation work in progress",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.08,
            "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.16,
            f"{self.sample_name} {self.sample_type} sample",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.24,
            "$f_c = $" + f"{self.f}",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )

        if self.save_path is not None:
            save_full_path = (
                f"{self.save_path}/{self.sample_type}_rejection_btag"
                if self.save_prefix is None
                else f"{self.save_path}/{self.save_prefix}_rejection_btag"
            )
            plt.savefig(f"{save_full_path}.pdf", bbox_inches="tight")
            plt.savefig(f"{save_full_path}.png", bbox_inches="tight")
            # plt.savefig(f"{self.save_path}/{self.sample_type}_rejection_btag.svg", bbox_inches="tight")

        plt.show()

        return rej_c, rej_l, _x

    def plot_discriminant(self, ylim: Tuple[float, float] = (1e-1, 1e10)) -> None:
        dict_Db = self.discriminant()
        _, _, Db_scaled_effs = self.get_rej_per_eff()

        fig, ax = plt.subplots(figsize=(8, 6))
        Db_max = max([max(dict_Db["b"]), max(dict_Db["c"]), max(dict_Db["l"])])
        Db_min = min([min(dict_Db["b"]), min(dict_Db["c"]), min(dict_Db["l"])])

        bins = np.linspace(Db_min, Db_max, 100)

        ax.hist(dict_Db["b"], bins=bins, label="b-jets", histtype="step")
        ax.hist(dict_Db["c"], bins=bins, label="c-jets", histtype="step")
        ax.hist(dict_Db["l"], bins=bins, label="l-jets", histtype="step")

        ax.set(
            xlabel="b-jets discriminant",
            ylabel="number of jets",
            yscale="log",
            ylim=ylim,
        )

        for k, v in self.dict_working_points.items():
            ax.axvline(
                x=Db_scaled_effs[int(v / self.eff_granularity)],
                ymax=v * 0.7,
                linestyle="dashed",
                linewidth=1,
                color="black",
            )
            ax.text(
                (Db_scaled_effs[int(v / self.eff_granularity)] - Db_min)
                / (Db_max - Db_min),
                v * 0.7 + 0.01,
                k,
                fontsize=10,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            # print(Db_scaled_effs[int(v/self.eff_granularity)])

        # LHC-ATLAS experiment text
        anchor_x = 0.02
        anchor_y = 0.9
        ax.text(
            anchor_x,
            anchor_y,
            "ATLAS",
            fontsize=15,
            weight="bold",
            fontstyle="italic",
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x + 0.12,
            anchor_y,
            "Simulation work in progress",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.08,
            "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        if self.sample_name == "$t\\bar{t}$":
            ax.text(
                anchor_x,
                anchor_y - 0.16,
                f"{self.sample_name} {self.sample_type} sample, "
                + "$20 < p_{T} < 250  \ \\mathrm{[GeV]}$",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
        elif self.sample_name == "$Z'$":
            ax.text(
                anchor_x,
                anchor_y - 0.16,
                f"{self.sample_name} {self.sample_type} sample, "
                + "$250 < p_{T} < 6000  \ \\mathrm{[GeV]}$",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
        ax.text(
            anchor_x,
            anchor_y - 0.24,
            "$f_c = $" + f"{self.f}",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )

        ax.legend(loc="upper right")

        if self.save_path is not None:
            save_full_path = (
                f"{self.save_path}/{self.sample_type}_discriminant_btag"
                if self.save_prefix is None
                else f"{self.save_path}/{self.save_prefix}_discriminant_btag"
            )
            plt.savefig(f"{save_full_path}.pdf", bbox_inches="tight")
            plt.savefig(f"{save_full_path}.png", bbox_inches="tight")
            # plt.savefig(f"{self.save_path}/{self.sample_type}_discriminant_btag.svg", bbox_inches="tight")

        plt.show()


class CTagEvaluator(TagEvaluator):
    def __init__(
        self,
        y_pred: np.ndarray,
        y_test: np.ndarray,
        sample_name: str,
        f: float,
        nbins: int = 1000,
        eff_granularity: float = 0.01,
        sample_type: Literal["training", "validation", "testing"] = "testing",
        save_path: Optional[str] = None,
        save_prefix: Optional[str] = None,
    ):
        super().__init__(
            y_pred,
            y_test,
            sample_name,
            f,
            nbins,
            eff_granularity,
            save_path,
            save_prefix,
        )
        self.mode = "c"
        self.sample_type = sample_type

    def plot_rejection(
        self, xlim=(0.2, 1), ylim=(1e0, 1e3)
    ) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        rej_b, rej_l, _ = self.get_rej_per_eff()
        fig, ax = plt.subplots(figsize=(8, 6))
        _x = np.arange(0, 1 + self.eff_granularity, self.eff_granularity)
        ax.plot(_x, rej_b, label="b-jets rejection")
        ax.plot(_x, rej_l, label="l-jets rejection")
        ax.set(
            xlim=xlim,
            xlabel="c-jets efficiency",
            ylabel="background rejection",
            yscale="log",
            ylim=ylim,
        )
        ax.legend()
        ax.grid()

        # LHC-ATLAS experiment text
        anchor_x = 0.15
        anchor_y = 0.9
        ax.text(
            anchor_x,
            anchor_y,
            "ATLAS",
            fontsize=15,
            weight="bold",
            fontstyle="italic",
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x + 0.12,
            anchor_y,
            "Simulation work in progress",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.08,
            "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.16,
            f"{self.sample_name} {self.sample_type} sample",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.24,
            "$f_b = $" + f"{self.f}",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )

        if self.save_path is not None:
            save_full_path = (
                f"{self.save_path}/{self.sample_type}_rejection_ctag"
                if self.save_prefix is None
                else f"{self.save_path}/{self.save_prefix}_rejection_ctag"
            )
            plt.savefig(f"{save_full_path}.pdf", bbox_inches="tight")
            plt.savefig(f"{save_full_path}.png", bbox_inches="tight")
            # plt.savefig(f"{self.save_path}/{self.sample_type}_rejection_ctag.svg", bbox_inches="tight")

        plt.show()

        return rej_b, rej_l, _x

    def plot_discriminant(self, ylim: Tuple[float, float] = (1e-1, 1e10)) -> None:
        dict_Dc = self.discriminant()
        _, _, Dc_scaled_effs = self.get_rej_per_eff()

        fig, ax = plt.subplots(figsize=(8, 6))
        Db_max = max([max(dict_Dc["b"]), max(dict_Dc["c"]), max(dict_Dc["l"])])
        Db_min = min([min(dict_Dc["b"]), min(dict_Dc["c"]), min(dict_Dc["l"])])

        bins = np.linspace(Db_min, Db_max, 100)

        ax.hist(dict_Dc["c"], bins=bins, label="c-jets", histtype="step")
        ax.hist(dict_Dc["b"], bins=bins, label="b-jets", histtype="step")
        ax.hist(dict_Dc["l"], bins=bins, label="l-jets", histtype="step")

        ax.set(
            xlabel="c-jets discriminant",
            ylabel="number of jets",
            yscale="log",
            ylim=ylim,
        )

        for k, v in self.dict_working_points.items():
            ax.axvline(
                x=Dc_scaled_effs[int(v / self.eff_granularity)],
                ymax=v * 0.7,
                linestyle="dashed",
                linewidth=1,
                color="black",
            )
            ax.text(
                (Dc_scaled_effs[int(v / self.eff_granularity)] - Db_min)
                / (Db_max - Db_min),
                v * 0.7 + 0.01,
                k,
                fontsize=10,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
            # print(Dc_scaled_effs[int(v/self.eff_granularity)])

        # LHC-ATLAS experiment text
        anchor_x = 0.02
        anchor_y = 0.9
        ax.text(
            anchor_x,
            anchor_y,
            "ATLAS",
            fontsize=15,
            weight="bold",
            fontstyle="italic",
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x + 0.12,
            anchor_y,
            "Simulation work in progress",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        ax.text(
            anchor_x,
            anchor_y - 0.08,
            "$\sqrt{s}=13 \ \mathrm{TeV}$, PFlow jets",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )
        if self.sample_name == "$t\\bar{t}$":
            ax.text(
                anchor_x,
                anchor_y - 0.16,
                f"{self.sample_name} {self.sample_type} sample, "
                + "$20 < p_{T} < 250  \ \\mathrm{[GeV]}$",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
        elif self.sample_name == "$Z'$":
            ax.text(
                anchor_x,
                anchor_y - 0.16,
                f"{self.sample_name} {self.sample_type} sample, "
                + "$250 < p_{T} < 6000  \ \\mathrm{[GeV]}$",
                fontsize=15,
                color="black",
                ha="left",
                va="bottom",
                transform=ax.transAxes,
            )
        ax.text(
            anchor_x,
            anchor_y - 0.24,
            "$f_b = $" + f"{self.f}",
            fontsize=15,
            color="black",
            ha="left",
            va="bottom",
            transform=ax.transAxes,
        )

        ax.legend(loc="upper right")

        if self.save_path is not None:
            save_full_path = (
                f"{self.save_path}/{self.sample_type}_discriminant_ctag"
                if self.save_prefix is None
                else f"{self.save_path}/{self.save_prefix}_discriminant_ctag"
            )
            plt.savefig(f"{save_full_path}.pdf", bbox_inches="tight")
            plt.savefig(f"{save_full_path}.png", bbox_inches="tight")
            # plt.savefig(f"{self.save_path}/{self.sample_type}_discriminant_ctag.svg", bbox_inches="tight")

        plt.show()


def bctag_evaluation(
    y_pred: np.ndarray,
    y_test: np.ndarray,
    sample_name: np.ndarray,
    fc: float,
    fb: float,
    save_path: Optional[str] = None,
    sample_type: Literal["training", "validation", "testing"] = "testing",
) -> None:
    with np.errstate(divide="ignore", invalid="ignore"):
        btag_evaluator = BTagEvaluator(
            y_pred,
            y_test,
            sample_name,
            fc,
            save_path=save_path,
            sample_type=sample_type,
        )
        rej_c, rej_l, eff_b = btag_evaluator.plot_rejection()
        btag_evaluator.plot_discriminant()

    # save rejection as csv
    df_rej = pd.DataFrame({"eff_b": eff_b, "rej_c": rej_c, "rej_l": rej_l})
    df_rej.to_csv(
        os.path.join(save_path, f"{sample_type}_rejection_btag.csv"), index=False
    )

    with np.errstate(divide="ignore", invalid="ignore"):
        ctag_evaluator = CTagEvaluator(
            y_pred,
            y_test,
            sample_name,
            fb,
            save_path=save_path,
            sample_type=sample_type,
        )
        rej_b, rej_l, eff_c = ctag_evaluator.plot_rejection()
        ctag_evaluator.plot_discriminant()

    # save rejection as csv
    df_rej = pd.DataFrame({"eff_c": eff_c, "rej_b": rej_b, "rej_l": rej_l})
    df_rej.to_csv(
        os.path.join(save_path, f"{sample_type}_rejection_ctag.csv"), index=False
    )


def eval_train_val_executor(
    mode: Literal["training", "validation"],
    dir_path: str,
    config,
    y_pred: np.ndarray,
    y_test: np.ndarray,
    total_loss: float,
    total_acc: float,
    epoch: int,
    experiment: Experiment = None,
    is_multi_task: Optional[bool] = False,
    dict_task_losses: Optional[Dict[str, np.ndarray]] = None,
    optional_working_point: Optional[Dict[Literal["b", "c"], float]] = None,
) -> Tuple[float, float, float, float]:
    if is_multi_task:
        loss_gc_mean = dict_task_losses["jet_classification"].mean()
        loss_nc_mean = dict_task_losses["track_origin"].mean()
        loss_ec_mean = dict_task_losses["vertexing"].mean()
    if not os.path.exists(os.path.join(dir_path, f"{epoch:03d}", "figure")):
        os.makedirs(os.path.join(dir_path, f"{epoch:03d}", "figure"))
    if mode == "training":
        if not os.path.exists(
            os.path.join(dir_path, f"{epoch:03d}", "figure", "training")
        ):
            os.makedirs(os.path.join(dir_path, f"{epoch:03d}", "figure", "training"))
    elif mode == "validation":
        if not os.path.exists(
            os.path.join(dir_path, f"{epoch:03d}", "figure", "validation")
        ):
            os.makedirs(os.path.join(dir_path, f"{epoch:03d}", "figure", "validation"))

    df_pred = pd.DataFrame(y_pred, columns=["pred_l", "pred_c", "pred_b"])
    df_test = pd.DataFrame(y_test, columns=["true_l", "true_c", "true_b"])
    df_concat = pd.concat([df_pred, df_test], axis=1)
    df_concat.to_csv(
        os.path.join(
            dir_path, f"{epoch:03d}", "figure", f"{mode}", f"{mode}_y_pred_y_test.csv"
        ),
        index=False,
    )

    bctag_evaluation(
        y_pred=y_pred,
        y_test=y_test,
        sample_name="hybrid",
        sample_type=mode,
        fc=config["metrics"]["fc"],
        fb=config["metrics"]["fb"],
        save_path=os.path.join(dir_path, f"{epoch:03d}", "figure", f"{mode}"),
    )

    btag_rejections = pd.read_csv(
        os.path.join(
            dir_path, f"{epoch:03d}", "figure", f"{mode}", f"{mode}_rejection_btag.csv"
        )
    )
    ctag_rejections = pd.read_csv(
        os.path.join(
            dir_path, f"{epoch:03d}", "figure", f"{mode}", f"{mode}_rejection_ctag.csv"
        )
    )

    if optional_working_point is None:
        btag_rej_c = btag_rejections["rej_c"][
            (btag_rejections["eff_b"] - config["metrics"]["working_point"]).abs() < 1e-3
        ].values[0]
        btag_rej_l = btag_rejections["rej_l"][
            (btag_rejections["eff_b"] - config["metrics"]["working_point"]).abs() < 1e-3
        ].values[0]
        ctag_rej_b = ctag_rejections["rej_b"][
            (ctag_rejections["eff_c"] - config["metrics"]["working_point"]).abs() < 1e-3
        ].values[0]
        ctag_rej_l = ctag_rejections["rej_l"][
            (ctag_rejections["eff_c"] - config["metrics"]["working_point"]).abs() < 1e-3
        ].values[0]
    else:
        btag_rej_c = btag_rejections["rej_c"][
            (btag_rejections["eff_b"] - optional_working_point["b"]).abs() < 1e-3
        ].values[0]
        btag_rej_l = btag_rejections["rej_l"][
            (btag_rejections["eff_b"] - optional_working_point["b"]).abs() < 1e-3
        ].values[0]
        ctag_rej_b = ctag_rejections["rej_b"][
            (ctag_rejections["eff_c"] - optional_working_point["c"]).abs() < 1e-3
        ].values[0]
        ctag_rej_l = ctag_rejections["rej_l"][
            (ctag_rejections["eff_c"] - optional_working_point["c"]).abs() < 1e-3
        ].values[0]
        with open(
            os.path.join(
                dir_path, f"{epoch:03d}", "figure", f"{mode}", f"working_points.yaml"
            ),
            "w",
        ) as f:
            import datetime
            import yaml

            # 日本時間の日付を2023-01-01 00:00:00の形式で取得
            today = datetime.datetime.now(
                datetime.timezone(datetime.timedelta(hours=9))
            )
            new_working_points = {
                "b": optional_working_point["b"],
                "c": optional_working_point["c"],
                "update": today.strftime("%Y-%m-%d %H:%M:%S"),
            }
            yaml.dump(new_working_points, f, default_flow_style=False)

    if experiment is not None:
        if is_multi_task:
            experiment.log_metric("total_loss", total_loss, step=epoch)
            experiment.log_metric("loss", loss_gc_mean, step=epoch)
            experiment.log_metric("loss_track-origin", loss_nc_mean, step=epoch)
            experiment.log_metric("loss_vertexing", loss_ec_mean, step=epoch)
        else:
            experiment.log_metric("loss", total_loss, step=epoch)
        experiment.log_metric("acc", total_acc, step=epoch)

        experiment.log_metric(f"btag_rej_c", btag_rej_c, step=epoch)
        experiment.log_metric(f"btag_rej_l", btag_rej_l, step=epoch)
        experiment.log_metric(f"ctag_rej_b", ctag_rej_b, step=epoch)
        experiment.log_metric(f"ctag_rej_l", ctag_rej_l, step=epoch)

    if total_acc is None:
        total_acc = 0.0
    if total_loss is None:
        total_loss = 100.0

    if is_multi_task:
        print(
            f"{mode}: total_loss {total_loss:.4f} ({loss_gc_mean:.4f}, {loss_nc_mean:.4f}, {loss_ec_mean:.4f}), acc {total_acc:.4f}, b_rej_c {btag_rej_c:.4f}, b_rej_l {btag_rej_l:.4f}, c_rej_c {ctag_rej_b:.4f}, c_rej_l {ctag_rej_l:.4f} wp {config['metrics']['working_point']}"
        )
    else:
        print(
            f"{mode}: loss {total_loss:.4f}, acc {total_acc:.4f}, b_rej_c {btag_rej_c:.4f}, b_rej_l {btag_rej_l:.4f}, c_rej_c {ctag_rej_b:.4f}, c_rej_l {ctag_rej_l:.4f} wp {config['metrics']['working_point']}"
        )
    return btag_rej_c, btag_rej_l, ctag_rej_b, ctag_rej_l
