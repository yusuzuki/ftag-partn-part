import random
import numpy as np
import torch


def fix_seed(seed: int = 42) -> None:
    """
    seedを固定する関数
    """
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed(seed)
