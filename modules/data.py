import os
import warnings

warnings.filterwarnings("ignore")
from typing import List, Tuple, Union, Dict, Any, Optional, Literal, Generator

import numpy as np
import pandas as pd
import yaml
import h5py
from tqdm import tqdm

import torch
from torch.utils.data import Dataset, ConcatDataset, TensorDataset, DataLoader

import umami
import umami.data_tools as udt
import umami.evaluation_tools as uet
import umami.tf_tools as utf
import umami.train_tools as utt
from umami.preprocessing_tools import get_scale_dict
from umami.evaluation_tools import feature_importance
from umami.helper_tools import get_class_label_variables, get_class_prob_var_names

DEBUG = False
PFLOW_TRAIN_PATH = "preprocessed/PFlow-hybrid-resampled_scaled_shuffled.h5"
PFLOW_VALIDATION_PATH = (
    "preprocessed/PFlow-hybrid-validation-resampled_scaled_shuffled.h5"
)
JETCLASS_TRAIN_PATH = "train.h5"
JETCLASS_VALIDATION_PATH = "validation.h5"


class H5Dataset(Dataset):
    """h5ファイルを逐次読み込み、データを返す
    see: https://qiita.com/oXyut/items/cb2b11f3ddf4b1018834
    """

    def __init__(
        self,
        dir_path: str,
        preload_factor: int,
        var_trks_idx: List[int],
        size: Optional[int] = None,
        transform=None,
        dtype=torch.float32,
        is_multi_task: bool = False,
    ):
        self.dir_path = dir_path
        self.size = size
        self.preload_factor = preload_factor
        self.transform = transform
        self.var_trks_idx = var_trks_idx
        self.h5_full_path = os.path.join(self.dir_path, PFLOW_TRAIN_PATH)
        self.dtype = dtype
        self.is_multi_task = is_multi_task

        if self.size is None:
            with h5py.File(self.h5_full_path, "r") as f:
                _full_size = len(f["tracks_loose"]["inputs"])
                print(f"full size {_full_size} is used.")
        else:
            print(f"size {self.size} is used.")

    def __len__(self):
        if self.size is None:
            with h5py.File(self.h5_full_path, "r") as f:
                full_size = len(f["tracks_loose"]["inputs"])
                return (full_size - 1) // self.preload_factor + 1
        else:
            return (self.size - 1) // self.preload_factor + 1

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor]:
        with h5py.File(self.h5_full_path, "r") as f:
            start = idx * self.preload_factor
            end = start + self.preload_factor
            x = f["tracks_loose"]["inputs"][start:end][:, :, self.var_trks_idx][
                ::
            ]  # (jets, tracks, features)
            y = f["jets"]["labels_one_hot"][start:end][::]
            if self.is_multi_task:
                truthOriginLabel = f["tracks_loose"]["labels"]["truthOriginLabel"][
                    start:end
                ][::]
                truthVertexIndex = f["tracks_loose"]["labels"]["truthVertexIndex"][
                    start:end
                ][::]

            x = x.transpose(0, 2, 1)  # (jets, features, tracks)

        if self.transform:
            x = self.transform(x)

        if self.is_multi_task:
            return (
                torch.tensor(x, dtype=self.dtype),
                torch.tensor(y, dtype=self.dtype),
                torch.tensor(truthOriginLabel, dtype=torch.int64),
                torch.tensor(truthVertexIndex),
            )
        else:
            return torch.tensor(x, dtype=self.dtype), torch.tensor(y, dtype=self.dtype)


class H5DatasetJetClass(Dataset):
    def __init__(
        self,
        train_full_path: str,
        var_trks_idx: List[int],
        size: Optional[int] = None,
        transform=None,
        dtype=torch.float32,
    ):
        self.train_full_path = train_full_path
        self.size = size
        self.transform = transform
        self.var_trks_idx = var_trks_idx
        self.dtype = dtype
        self.n_data_per_file = 1000000

        if self.size is None:
            with h5py.File(self.train_full_path, "r") as f:
                _full_size = len(f["features"]) * self.n_data_per_file
                print(f"full size {_full_size} is used.")
        else:
            print(f"size {self.size} is used.")

    def __len__(self):
        if self.size is None:
            with h5py.File(self.train_full_path, "r") as f:
                full_size = len(f["features"]) * self.n_data_per_file
                return (full_size - 1) // self.n_data_per_file + 1
        else:
            return (self.size - 1) // self.n_data_per_file + 1

    def __getitem__(self, idx: int) -> Tuple[torch.Tensor, torch.Tensor, torch.Tensor]:
        # print(f"loading {idx}th file...")
        with h5py.File(self.train_full_path, "r") as f:
            x = f["features"][f"{idx:03d}"][:, :, self.var_trks_idx][
                ::
            ]  # (jets, tracks, features)
            y = f["labels_one_hot"][f"{idx:03d}"][::]
            mask = f["masks"][f"{idx:03d}"][::]

            x = x.transpose(0, 2, 1)  # (jets, features, tracks)

        if self.transform:
            x = self.transform(x)

        return (
            torch.tensor(x, dtype=self.dtype),
            torch.tensor(y, dtype=self.dtype),
            torch.tensor(mask, dtype=torch.bool),
        )


class OutMemoryDataLoader:
    """メモリに乗り切らない大きなデータセットを読み込むためのDataLoader
    see: https://qiita.com/oXyut/items/cb2b11f3ddf4b1018834
    """

    def __init__(
        self,
        dir_path: str,
        preload_factor: int,
        var_trks_use: List[str],
        var_trks_all: List[str],
        size: Optional[int] = None,
        num_workers: int = 1,
        dtype=torch.float32,
        is_jet_class: bool = False,
        is_multi_task: bool = False,
    ):
        self.dir_path = dir_path
        self.preload_factor = preload_factor
        self.var_trks_use = var_trks_use
        self.var_trks_all = var_trks_all
        self.var_trks_idx = [self.var_trks_all.index(var) for var in self.var_trks_use]
        self.size = size
        self.num_workers = num_workers
        self.dtype = dtype
        self.is_multi_task = is_multi_task
        if is_jet_class:
            self.dataset = H5DatasetJetClass(
                self.dir_path, self.var_trks_idx, size=self.size, dtype=self.dtype
            )
            print("dataset length: ", len(self.dataset))
        else:
            self.dataset = H5Dataset(
                self.dir_path,
                self.preload_factor,
                self.var_trks_idx,
                size=self.size,
                dtype=self.dtype,
                is_multi_task=self.is_multi_task,
            )

        print("training data size:", self.size)

    def dataloader(self) -> DataLoader:
        return DataLoader(
            self.dataset,
            batch_size=1,
            shuffle=False,
            num_workers=self.num_workers,
            collate_fn=lambda batch: batch[0],
        )


class InMemoryDataLoader:
    def __init__(
        self,
        is_validation: bool,
        dir_path: str,
        batch_size: int,
        var_trks_use: List[str],
        var_trks_all: List[str],
        size: Optional[int] = None,
        num_workers: int = 1,
        dtype=torch.float32,
        is_multi_task: bool = False,
    ):
        self.is_validation = is_validation
        self.dir_path = dir_path
        self.batch_size = batch_size
        self.var_trks_use = var_trks_use
        self.num_workers = num_workers
        self.var_trks_all = var_trks_all
        self.var_trks_idx = [self.var_trks_all.index(var) for var in self.var_trks_use]
        self.h5_full_path = (
            os.path.join(self.dir_path, PFLOW_VALIDATION_PATH)
            if self.is_validation
            else os.path.join(self.dir_path, PFLOW_TRAIN_PATH)
        )
        with h5py.File(self.h5_full_path, "r") as f:
            self.total_size = len(f["tracks_loose"]["inputs"])
        self.size = self.total_size if size is None else size
        self.dtype = dtype
        self.is_multi_task = is_multi_task

        if is_validation:
            print("validation data size:", self.size)
        else:
            print("training data size:", self.size)

    @property
    def dataset(self) -> Dataset:
        with h5py.File(self.h5_full_path, "r") as f:
            x = f["tracks_loose"]["inputs"][: self.size][:, :, self.var_trks_idx][::]
            y = f["jets"]["labels_one_hot"][: self.size][::]
            if self.is_multi_task:
                truthOriginLabel = f["tracks_loose"]["labels"]["truthOriginLabel"][
                    : self.size
                ][::]
                truthVertexIndex = f["tracks_loose"]["labels"]["truthVertexIndex"][
                    : self.size
                ][::]

        if self.is_multi_task:
            return TensorDataset(
                torch.tensor(x.transpose(0, 2, 1), dtype=self.dtype),
                torch.tensor(y, dtype=self.dtype),
                torch.tensor(truthOriginLabel, dtype=torch.int64),
                torch.tensor(truthVertexIndex, dtype=torch.int64),
            )
        else:
            return TensorDataset(
                torch.tensor(x.transpose(0, 2, 1), dtype=self.dtype),
                torch.tensor(y, dtype=self.dtype),
            )

    def dataloader(self) -> DataLoader:
        return DataLoader(
            self.dataset,
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )


class InMemoryDataLoaderOld:
    def __init__(
        self,
        is_validation: bool,
        dir_path: str,
        batch_size: int,
        var_trks_use: List[str],
        var_trks_all: List[str],
        size: int,
        num_workers: int = 1,
    ):
        self.is_validation = is_validation
        self.dir_path = dir_path
        self.batch_size = batch_size
        self.var_trks_use = var_trks_use
        if size > 5000000:
            raise Warning(f"size {size} is too large. It may cause memory error.")
        self.size = size
        self.num_workers = num_workers
        self.var_trks_all = var_trks_all
        self.var_trks_idx = [self.var_trks_all.index(var) for var in self.var_trks_use]
        self.h5_full_path = os.path.join(self.dir_path, PFLOW_TRAIN_PATH)
        with h5py.File(self.h5_full_path, "r") as f:
            self.total_size = len(f["tracks_loose"]["inputs"])

    def dataset(self) -> Dataset:
        with h5py.File(self.h5_full_path, "r") as f:
            if self.is_validation:
                # validation dataは後ろから取る
                x = f["tracks_loose"]["inputs"][(self.total_size - self.size) :][
                    :, :, self.var_trks_idx
                ][::]
                y = f["jets"]["labels_one_hot"][(self.total_size - self.size) :][::]
            else:
                # training dataは前から取る
                x = f["tracks_loose"]["inputs"][: self.size][:, :, self.var_trks_idx][
                    ::
                ]
                y = f["jets"]["labels_one_hot"][: self.size][::]

        x = torch.tensor(
            x.transpose(0, 2, 1), dtype=torch.float32
        )  # (jets, features, tracks)
        y = torch.tensor(y, dtype=torch.float32)
        return TensorDataset(x, y)

    def dataloader(self) -> DataLoader:
        return DataLoader(
            self.dataset(),
            batch_size=self.batch_size,
            shuffle=False,
            num_workers=self.num_workers,
        )


class TestDataset:
    def __init__(
        self,
        training_config_dir: str,
        test_dataset_name: str,
        var_trks_use: List[str],
        var_trks_all: List[str],
    ):
        self.training_config_dir = training_config_dir
        self.test_dataset_name = test_dataset_name
        self.training_config_path = os.path.join(
            self.training_config_dir, "Dips-PFlow-Training-config.yaml"
        )
        self.train_config = utt.TrainConfiguration(self.training_config_path)
        # print(self.train_config.general.preprocess_config.dict_file)
        self.var_trks_use = var_trks_use
        with open(self.training_config_path) as f:
            self.training_config = yaml.safe_load(f)
        self.var_trks_all = var_trks_all
        self.var_trks_idx = [self.var_trks_all.index(v) for v in self.var_trks_use]
        self.coord_idx = [self.var_trks_use.index(v) for v in ["deta", "dphi"]]

    def load_test_h5(
        self, with_jet: bool = False, is_plot: bool = False
    ) -> Tuple[np.ndarray, np.ndarray]:
        X, y_test = utt.get_test_sample_trks(
            input_file=self.train_config.general.test_files[self.test_dataset_name][
                "path"
            ],
            var_dict=self.train_config.general.var_dict,
            scale_dict=self.train_config.general.preprocess_config.general.dict_file,
            class_labels=self.train_config.nn_structure.class_labels
            + self.train_config.evaluation_settings.extra_classes_to_evaluate,
            tracks_name=self.train_config.general.tracks_name,
            n_jets=int(self.train_config.evaluation_settings.n_jets),
            cut_vars_dict=self.train_config.general.test_files[
                self.test_dataset_name
            ].get("variable_cuts"),
        )  # X: (n_jets, n_tracks, n_features), y_test: (n_jets, n_classes)

        if not with_jet:
            return X[:, :, self.var_trks_idx].transpose(0, 2, 1), y_test
        else:
            X_jet, y_test_jet = utt.get_test_sample(
                input_file=self.train_config.general.test_files[self.test_dataset_name][
                    "path"
                ],
                var_dict=self.train_config.general.var_dict,
                scale_dict=self.train_config.general.preprocess_config.general.dict_file,
                class_labels=self.train_config.nn_structure.class_labels
                + self.train_config.evaluation_settings.extra_classes_to_evaluate,
                indices_to_load=None,
                n_jets=int(self.train_config.evaluation_settings.n_jets),
                cut_vars_dict=self.train_config.general.test_files[
                    self.test_dataset_name
                ].get("variable_cuts"),
            )
            print(
                "check jet-track consistency: ", np.all(y_test_jet == y_test)
            ) if DEBUG else None
            print("X of tracks", X.shape) if DEBUG else None
            print("X of jets", X_jet.shape) if DEBUG else None
            X_jet = X_jet.values
            valid = (X[:, :, 0] != 0.0).astype(bool)
            X_jet = X_jet[:, np.newaxis, :].repeat(40, axis=1)
            X_jet = X_jet * valid[:, :, np.newaxis]
            print("X of jets (tiled)", X_jet.shape) if DEBUG else None
            return np.concatenate([X_jet, X], axis=-1).transpose(0, 2, 1), y_test

    def tensor(self, with_jet: bool = False) -> Tuple[torch.Tensor, torch.Tensor]:
        X, y_test = self.load_test_h5(with_jet=with_jet)
        X = torch.from_numpy(X).float()
        y_test = torch.from_numpy(y_test).float()

        return X, y_test

    def dataloader(
        self, batch_size: int, with_jet: bool = False
    ) -> Tuple[DataLoader, torch.Tensor]:
        X, y_test = self.tensor(with_jet=with_jet)
        dataloader = DataLoader(
            TensorDataset(X, y_test),
            batch_size=batch_size,
            shuffle=False,
        )

        for _x, _y in dataloader:
            print(f"X: {_x.shape}")
            print(f"y: {_y.shape}")
            break

        return dataloader, y_test

    def dataloader_from_file(
        self, dir_path, batch_size=128
    ) -> Tuple[DataLoader, torch.Tensor]:
        print("trying to load test dataset from file...")
        with open(os.path.join("./test_dataset", f"var_trks_use.txt"), "r") as f:
            var_trks_use_load = [var.strip() for var in f.readlines()]
        if self.var_trks_use != var_trks_use_load:
            raise ValueError("var_trks_use is not same as var_trks_use_load")
        X = torch.load(os.path.join(dir_path, self.test_dataset_name, "X.pt"))
        y_test = torch.load(os.path.join(dir_path, self.test_dataset_name, "y_test.pt"))
        dataloader = DataLoader(
            TensorDataset(X, y_test),
            batch_size=batch_size,
            shuffle=False,
        )

        for _x, _y in dataloader:
            print(f"X: {_x.shape}")
            print(f"y: {_y.shape}")
            break

        return dataloader, y_test


class TestDataset2Old:
    def __init__(
        self,
        data_path: str,
        dataset_name: Literal["ttbar", "zprime"],
        var_trks_use: List[str],
        scale_dict,
        batch_size: int,
        size: Optional[int] = None,
    ):
        """
        umamiを使用せずに直接テストデータを作成するクラス
        メモリをたくさん食ってしまうためボツ

        Args:
            data_path (str): path to data directory
            dataset_name (Literal["ttbar", "zprime"]): dataset name
            var_trks_use (List[str]): track variables to use
            size (Optional[int], optional): number of jets to use. Defaults to None.
        """
        self.data_path = data_path
        self.dataset_name = dataset_name
        self.var_trks_use = var_trks_use.copy()
        self.test_file_path = os.path.join(
            self.data_path,
            "prepared",
            f"inclusive_testing_{self.dataset_name}_PFlow.h5",
        )
        if not os.path.exists(self.test_file_path):
            raise ValueError(f"{self.test_file_path} does not exist.")
        self.batch_size = batch_size
        self.size = size
        self.allowed_flavors = [0, 4, 5]  # light, c, b
        self.flavour_variable_name = "HadronConeExclTruthLabelID"
        self.margin_rate = 1.05
        self.jet_variables = []
        if "eta_btagJes" in self.var_trks_use:
            self.jet_variables.append("eta_btagJes")
            self.var_trks_use.remove("eta_btagJes")
        if "pt_btagJes" in self.var_trks_use:
            self.jet_variables.append("pt_btagJes")
            self.var_trks_use.remove("pt_btagJes")
        if len(self.jet_variables) == 0:
            raise ValueError("jet_variableが空の場合は未実装だよーん")
        self.scale_dict = scale_dict

    @property
    def selected_data(self) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
        """
        Returns:
            Tuple[np.ndarray, np.ndarray, np.ndarray]: (concat_jet_tracks, labels, jet_pt)
        """
        with h5py.File(self.test_file_path, "r") as f:
            jets = (
                f["jets"][: int(self.size * self.margin_rate)][:]
                if self.size
                else f["jets"][:]
            )
            tracks = (
                f["tracks_loose"][: int(self.size * self.margin_rate)][:]
                if self.size
                else f["tracks_loose"][:]
            )

        judge = np.isin(jets[self.flavour_variable_name], self.allowed_flavors)
        jets = jets[judge][: self.size] if self.size else jets[judge]
        tracks = tracks[judge][: self.size] if self.size else tracks[judge]

        print(f"selected {len(jets)} jets.")

        jet_pt = jets["pt"]

        labels = jets[self.flavour_variable_name]
        labels = np.stack([labels == i for i in self.allowed_flavors], axis=1)

        tracks_valid = tracks["valid"]

        jets = np.vstack(
            [
                (jets[var].astype(float) - self.scale_dict["jets"][var]["shift"])
                / self.scale_dict["jets"][var]["scale"]
                for var in self.jet_variables
            ]
        ).T
        tracks = np.stack(
            [
                (
                    tracks[var].astype(float)
                    - self.scale_dict["tracks_loose"][var]["shift"]
                )
                / self.scale_dict["tracks_loose"][var]["scale"]
                for var in self.var_trks_use
            ],
            axis=1,
        )
        # concat jet: (n_jets, n_jet_variables) -> (n_jets, n_jet_variables, n_tracks)
        jets = np.repeat(jets[:, :, np.newaxis], tracks.shape[-1], axis=2)
        concat_jet_tracks = np.concatenate([jets, tracks], axis=1)
        # tracks_valid: (n_jets, n_tracks) -> (n_jets, n_variables, n_tracks)
        tracks_valid = np.repeat(
            tracks_valid[:, np.newaxis, :],
            len(self.jet_variables) + len(self.var_trks_use),
            axis=1,
        )
        concat_jet_tracks = np.where(tracks_valid, concat_jet_tracks, 0)
        return concat_jet_tracks, labels, jet_pt

    @property
    def tensor(self) -> Tuple[torch.Tensor, torch.Tensor, np.ndarray]:
        """
        Returns:
            Tuple[torch.Tensor, torch.Tensor, np.ndarray]: (X, y, pt)
        """
        X, y, pt = self.selected_data
        return (
            torch.tensor(X, dtype=torch.float32),
            torch.tensor(y, dtype=torch.float32),
            pt,
        )

    @property
    def dataloader_with_info(self) -> Tuple[DataLoader, np.ndarray, np.ndarray]:
        """
        Returns:
            Tuple[DataLoader, np.ndarray, np.ndarray]: (dataloader, y, pt)
        """
        X, y, pt = self.tensor
        return (
            DataLoader(TensorDataset(X, y), batch_size=self.batch_size, shuffle=False),
            y.detach().numpy(),
            pt,
        )


class TestDataset2:
    def __init__(
        self,
        data_path: str,
        dataset_name: Literal["ttbar", "zprime"],
        var_trks_use: List[str],
        scale_dict,
        batch_size: int,
        size: Optional[int] = None,
        meta_jet_variables: List[str] = ["pt", "eta", "HadronConeExclTruthLabelID"],
        is_multi_task: bool = False,
    ):
        """
        umamiを使用せずに直接テストデータを作成するクラス

        Args:
            data_path (str): path to data directory
            dataset_name (Literal["ttbar", "zprime"]): dataset name
            var_trks_use (List[str]): track variables to use
            size (Optional[int], optional): number of jets to use. Defaults to None.
            meta_jet_variables (List[str], optional): jet variables to use. Defaults to ["pt", "eta", "HadronConeExclTruthLabelID"].
            is_multi_task (bool, optional): whether to use multi-task. Defaults to False.
        """
        self.data_path = data_path
        self.dataset_name = dataset_name
        self.var_trks_use = var_trks_use.copy()
        self.test_file_path = os.path.join(
            self.data_path,
            "prepared",
            f"inclusive_testing_{self.dataset_name}_PFlow.h5",
        )
        if not os.path.exists(self.test_file_path):
            raise ValueError(f"{self.test_file_path} does not exist.")
        self.batch_size = batch_size
        self.size = size
        self.allowed_flavors = [0, 4, 5]  # light, c, b
        self.flavour_variable_name = "HadronConeExclTruthLabelID"
        self.jet_variables = []
        if "eta_btagJes" in self.var_trks_use:
            self.jet_variables.append("eta_btagJes")
            self.var_trks_use.remove("eta_btagJes")
        if "pt_btagJes" in self.var_trks_use:
            self.jet_variables.append("pt_btagJes")
            self.var_trks_use.remove("pt_btagJes")
        if len(self.jet_variables) == 0:
            raise ValueError("jet_variableが空の場合は未実装だよーん")
        self.scale_dict = scale_dict
        self.meta_jet_variables = meta_jet_variables
        self.is_multi_task = is_multi_task

    def data_generator(
        self,
    ) -> Generator[Tuple[np.ndarray, np.ndarray, np.ndarray], None, None]:
        with h5py.File(self.test_file_path, "r") as f:
            total_size = len(f["jets"]) if self.size is None else self.size
            num_batches = total_size // self.batch_size

            for i in range(num_batches + 1):
                start_idx = i * self.batch_size
                end_idx = start_idx + self.batch_size
                if end_idx > total_size:
                    end_idx = total_size

                jets = f["jets"][start_idx:end_idx]
                tracks = f["tracks_loose"][start_idx:end_idx]

                if self.is_multi_task:
                    truthOriginLabel = tracks["truthOriginLabel"].astype(int)
                    truthVertexIndex = tracks["truthVertexIndex"].astype(int)

                labels = jets[self.flavour_variable_name]
                labels = np.stack([labels == i for i in self.allowed_flavors], axis=1)

                tracks_valid = tracks["valid"]

                meta_jet = np.stack(
                    [jets[var] for var in self.meta_jet_variables], axis=1
                )
                jets = np.vstack(
                    [
                        (
                            jets[var].astype(float)
                            - self.scale_dict["jets"][var]["shift"]
                        )
                        / self.scale_dict["jets"][var]["scale"]
                        for var in self.jet_variables
                    ]
                ).T
                tracks = np.stack(
                    [
                        (
                            tracks[var].astype(float)
                            - self.scale_dict["tracks_loose"][var]["shift"]
                        )
                        / self.scale_dict["tracks_loose"][var]["scale"]
                        for var in self.var_trks_use
                    ],
                    axis=1,
                )
                # concat jet: (n_jets, n_jet_variables) -> (n_jets, n_jet_variables, n_tracks)
                jets = np.repeat(jets[:, :, np.newaxis], tracks.shape[-1], axis=2)
                concat_jet_tracks = np.concatenate([jets, tracks], axis=1)
                # tracks_valid: (n_jets, n_tracks) -> (n_jets, n_variables, n_tracks)
                tracks_valid = np.repeat(
                    tracks_valid[:, np.newaxis, :],
                    len(self.jet_variables) + len(self.var_trks_use),
                    axis=1,
                )
                concat_jet_tracks = np.where(tracks_valid, concat_jet_tracks, 0)

                if self.is_multi_task:
                    yield concat_jet_tracks, labels, meta_jet, truthOriginLabel, truthVertexIndex
                else:
                    yield concat_jet_tracks, labels, meta_jet

    def create_dataloader_metadata(self) -> Tuple[DataLoader, pd.DataFrame]:
        dataset_list = []
        list_metadata = []
        if self.is_multi_task:
            for (
                concat_jet_tracks,
                labels,
                meta_jet,
                truthOriginLabel,
                truthVertexIndex,
            ) in tqdm(self.data_generator()):
                tensor_data = TensorDataset(
                    torch.tensor(concat_jet_tracks, dtype=torch.float32),
                    torch.tensor(labels, dtype=torch.float32),
                    torch.tensor(truthOriginLabel, dtype=torch.int64),
                    torch.tensor(truthVertexIndex, dtype=torch.int64),
                )
                dataset_list.append(tensor_data)
                list_metadata.append(meta_jet)
        else:
            for concat_jet_tracks, labels, meta_jet in tqdm(self.data_generator()):
                tensor_data = TensorDataset(
                    torch.tensor(concat_jet_tracks, dtype=torch.float32),
                    torch.tensor(labels, dtype=torch.float32),
                )
                dataset_list.append(tensor_data)
                list_metadata.append(meta_jet)

        concatenated_dataset = ConcatDataset(dataset_list)
        metadata = pd.DataFrame(
            np.concatenate(list_metadata, axis=0), columns=self.meta_jet_variables
        )

        return (
            DataLoader(concatenated_dataset, batch_size=self.batch_size, shuffle=False),
            metadata,
        )

    # def get_matadata(self) -> pd.DataFrame:
    #     list_metadata = []
    #     if self.is_multi_task:
    #         for _, _, meta_jet, _, _ in tqdm(self.data_generator()):
    #             list_metadata.append(meta_jet)
    #         metadata = np.concatenate(list_metadata, axis=0)
    #         return pd.DataFrame(metadata, columns=self.meta_jet_variables)
    #     else:
    #         for _, _, meta_jet in tqdm(self.data_generator()):
    #             list_metadata.append(meta_jet)
    #         metadata = np.concatenate(list_metadata, axis=0)
    #         return pd.DataFrame(metadata, columns=self.meta_jet_variables)
