from typing import Union
import h5py


def _display_recursive(
    h5_object: Union[h5py.Dataset, h5py.Group], indent: str = ""
) -> None:
    if isinstance(h5_object, h5py.Dataset):
        print(indent + h5_object.name + ": [Dataset]" + str(h5_object.shape))
    elif isinstance(h5_object, h5py.Group):
        print(indent + h5_object.name + ": [Group]")
        for key in h5_object.keys():
            _display_recursive(h5_object[key], indent + "  ")
    else:
        raise TypeError("Unsupported object type: %s" % type(h5_object))


def display_h5_file(filename: str) -> None:
    """h5ファイルの中身を再帰的に表示する関数

    Args:
        filename (str): h5ファイル名

    Examples:
        >>> display_h5_file('data.h5')

    Note:
        Datasetが大きい場合は表示に時間がかかる。

    """
    with h5py.File(filename, "r") as h5_file:
        print(f"Displaying contents of file: {filename}")
        for key in h5_file.keys():
            _display_recursive(h5_file[key])


def flatten_dict(d: dict, parent_key="", sep="/") -> dict:
    """ネストされたdictをフラットなdictに変換する関数

    Args:
        d (dict): ネストされたdict
        parent_key (str, optional): ネストされたdictの親key. Defaults to ''.
        sep (str, optional): フラットなdictのkeyの区切り文字. Defaults to '_'.

    Returns:
        dict: フラットなdict

    Examples:
        >>> d = {'a': {'b': 1, 'c': 2}, 'd': {'e': 3, 'f': 4}}

    Note:
        ネストされたdictのkeyにsepが含まれている場合はエラーになる。
    """
    items = []
    for k, v in d.items():
        new_key = f"{parent_key}{sep}{k}" if parent_key else k
        if isinstance(v, dict):
            items.extend(flatten_dict(v, new_key, sep=sep).items())
        else:
            items.append((new_key, v))
    return dict(items)


def flatten_dict_parent_key_removed(d: dict, sep="/") -> dict:
    d2 = flatten_dict(d, sep=sep)
    # parent_keyを削除
    d3 = {}
    for k, v in d2.items():
        d3[k.split(sep)[-1]] = v
    return d3
