import os
from typing import Tuple, Dict, List, Union, Any, Optional, Literal, MutableSequence
import dataclasses

import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from modules import eval
from puma import Roc, RocPlot, VarVsEff, VarVsEffPlot, Line2D, Line2DPlot
from puma.metrics import calc_eff


class Comparator:
    def __init__(self, config, sample: Literal["ttbar", "zprime"]):
        self.config = config
        self.sample = sample
        self.model_names = [_model["name"] for _model in self.config["models"]]
        self.dict_sample_name_tex = {"ttbar": "$t\\bar{t}$", "zprime": "$Z'$"}

        for model in self.config["models"]:
            if "meta" in model.keys():
                print(f"meta model:\t{model['name']}")
            elif "reference" in model.keys():
                print(f"ref. model:\t{model['name']}")

    def _get_model(self, model_key) -> str:
        for model in self.config["models"]:
            if model_key in model.keys() and model[model_key]:
                return model["name"]
        # if not found
        raise ValueError(f"No {model_key} model")

    @property
    def reference_model(self):
        return self._get_model("reference")

    @property
    def meta_model(self):
        return self._get_model("meta")

    def __add_Db(
        df: pd.DataFrame, model_name: Literal["partn", "dips", "GN1"], fc: float = 0.018
    ) -> pd.DataFrame:
        df[f"Db_{model_name}"] = np.log(
            df[f"{model_name}_pb"]
            / ((1 - fc) * df[f"{model_name}_pu"] + fc * df[f"{model_name}_pc"])
        )
        df["fc"] = fc
        return df

    def __add_Dc(
        df: pd.DataFrame, model_name: Literal["partn", "dips", "GN1"], fb: float = 0.20
    ) -> pd.DataFrame:
        df[f"Dc_{model_name}"] = np.log(
            df[f"{model_name}_pc"]
            / ((1 - fb) * df[f"{model_name}_pu"] + fb * df[f"{model_name}_pb"])
        )
        df["fb"] = fb
        return df

    @property
    def df_concat(self):
        dict_df = {}
        for model in self.config["models"]:
            if model["type"] == "original":
                _df = pd.read_csv(model["prediction_path"][self.sample])
                if model["name"] == self.meta_model:
                    _df = _df[
                        [
                            "pred_b",
                            "pred_c",
                            "pred_l",
                            "HadronConeExclTruthLabelID",
                            "pt",
                            "eta",
                        ]
                    ]
                    _df.columns = [
                        f"{model['name']}_pb",
                        f"{model['name']}_pc",
                        f"{model['name']}_pu",
                        "HadronConeExclTruthLabelID",
                        "pt",
                        "eta",
                    ]
                    _df["n_tracks"] = np.load(self.config["n_tracks_npy"][self.sample])
                else:
                    _df = _df[["pred_b", "pred_c", "pred_l"]]
                    _df.columns = [
                        f"{model['name']}_pb",
                        f"{model['name']}_pc",
                        f"{model['name']}_pu",
                    ]
                dict_df[model["name"]] = _df
            elif model["type"] == "salt":
                if "alias" in model.keys():
                    _model_name = model["alias"]
                else:
                    _model_name = model["name"]
                with h5py.File(model["prediction_path"][self.sample], "r") as f:
                    _df = pd.DataFrame(f["jets"][:])
                if model["name"] == self.meta_model:
                    _df = _df[
                        [
                            f"{_model_name}_pb",
                            f"{_model_name}_pc",
                            f"{_model_name}_pu",
                            "HadronConeExclTruthLabelID",
                            "pt",
                            "eta",
                        ]
                    ]
                    _df.columns = [
                        f"{model['name']}_pb",
                        f"{model['name']}_pc",
                        f"{model['name']}_pu",
                        "HadronConeExclTruthLabelID",
                        "pt",
                        "eta",
                    ]
                else:
                    _df = _df[
                        [f"{_model_name}_pb", f"{_model_name}_pc", f"{_model_name}_pu"]
                    ]
                    _df.columns = [
                        f"{model['name']}_pb",
                        f"{model['name']}_pc",
                        f"{model['name']}_pu",
                    ]
                dict_df[model["name"]] = _df

        # concatする前にdfのlengthが全て同じかチェック
        _len = len(dict_df[self.reference_model])
        for model in dict_df.keys():
            if len(dict_df[model]) != _len:
                raise ValueError(
                    f"Length of {model}:{len(dict_df[model])} is not same as reference model: {_len}"
                )

        df_concat = pd.concat(dict_df.values(), axis=1)
        # cut nan
        print("Before cut nan: ", len(df_concat))
        df_concat = df_concat.dropna()
        print("After cut nan: ", len(df_concat))
        # cut by confition
        if "cut" in self.config.keys() and self.sample in self.config["cut"].keys():
            query = self.config["cut"][self.sample]
            print("Before cut by condition: ", len(df_concat))
            df_concat = df_concat.query(query)
            print("After cut by condition: ", len(df_concat))
        else:
            print("No cut by condition")

        self.n_jets = len(df_concat)

        # add discriminant
        for model in dict_df.keys():
            df_concat = Comparator.__add_Db(
                df_concat, model_name=model, fc=self.config["params"]["disc_f"]["fc"]
            )
            df_concat = Comparator.__add_Dc(
                df_concat, model_name=model, fb=self.config["params"]["disc_f"]["fb"]
            )

        return df_concat

    def __plot_model_discriminant_rejection(
        self,
        model_name: str,
        tagger: Literal["btag", "ctag", "both"],
        df_concat: pd.DataFrame,
        sample_name: Literal["ttbar", "zprime"],
        save_path: Optional[str] = None,
    ) -> Dict[str, np.ndarray]:
        y_test = np.stack(
            [
                df_concat["HadronConeExclTruthLabelID"].values == 0,
                df_concat["HadronConeExclTruthLabelID"].values == 4,
                df_concat["HadronConeExclTruthLabelID"].values == 5,
            ],
            axis=1,
        ).astype(int)

        dict_rej = {}

        if tagger == "btag" or tagger == "both":
            b_tag_evaluator = eval.BTagEvaluator(
                y_pred=df_concat[
                    [f"{model_name}_{p}" for p in ["pu", "pc", "pb"]]
                ].values,
                y_test=y_test,
                sample_name=self.dict_sample_name_tex[sample_name],
                f=self.config["params"]["disc_f"]["fc"],
                save_path=save_path,
                save_prefix=model_name,
            )
            b_tag_evaluator.plot_discriminant()
            rej_c, rej_l, effs = b_tag_evaluator.plot_rejection()
            dict_rej["btag"] = {"eff_b": effs, "rej_c": rej_c, "rej_l": rej_l}

        if tagger == "ctag" or tagger == "both":
            ctag_evaluator = eval.CTagEvaluator(
                y_pred=df_concat[
                    [f"{model_name}_{p}" for p in ["pu", "pc", "pb"]]
                ].values,
                y_test=y_test,
                sample_name=self.dict_sample_name_tex[sample_name],
                f=self.config["params"]["disc_f"]["fb"],
                save_path=save_path,
                save_prefix=model_name,
            )
            ctag_evaluator.plot_discriminant()
            rej_c, rej_l, effs = ctag_evaluator.plot_rejection()
            dict_rej["ctag"] = {"eff_c": effs, "rej_b": rej_c, "rej_l": rej_l}

        return dict_rej

    def plot_models_discriminant_rejection(
        self,
        tagger: Literal["btag", "ctag", "both"],
        save_path: Optional[str] = None,
    ) -> Dict[str, Dict[str, Dict[str, np.ndarray]]]:
        dict_rej = {}
        _df_concat = self.df_concat
        for model in self.model_names:
            dict_rej[model] = self.__plot_model_discriminant_rejection(
                model_name=model,
                tagger=tagger,
                df_concat=_df_concat,
                sample_name=self.sample,
                save_path=save_path,
            )
        return dict_rej

    def get_rej_at_wp(self, wp: float, dict_rej=None):
        dict_rej = (
            self.plot_models_discriminant_rejection(tagger="both")
            if dict_rej is None
            else dict_rej
        )
        dict_rej_at_wp = {}
        for model in self.model_names:
            dict_rej_at_wp[model] = {}
            for tagger in dict_rej[model].keys():
                dict_rej_at_wp[model][tagger] = {}
                for key in dict_rej[model][tagger].keys():
                    dict_rej_at_wp[model][tagger][key] = dict_rej[model][tagger][key][
                        np.argmin(np.abs(dict_rej[model][tagger][key] - wp))
                    ]
        return dict_rej_at_wp


class ComparatorPlotter:
    def __init__(self, comparator: Comparator, save_dir: str):
        self.comparator = comparator
        self.config = comparator.config
        self.sample = comparator.sample
        self.model_names = comparator.model_names
        self.reference_model = comparator.reference_model
        self.meta_model = comparator.meta_model
        self.df_concat = comparator.df_concat
        self.n_jets = comparator.n_jets
        self.dict_sample_name_tex = {"ttbar": "$t\\bar{t}$", "zprime": "$Z'$"}
        self.dict_rej_models = self.comparator.plot_models_discriminant_rejection(
            tagger="both"
        )
        self.save_dir = save_dir
        if not os.path.exists(self.save_dir):
            os.makedirs(self.save_dir)
            print(f"Created {self.save_dir}")

    def plot_btag_rejection_comparison(
        self,
        lim_eff: Tuple[float, float] = (0.6, 1.0),
    ):
        dict_rej_models = self.dict_rej_models
        n_jets = self.n_jets
        atlas_first_tag = self.config["atlas_tag"]["atlas_first_tag"]
        atlas_second_tag = self.config["atlas_tag"]["atlas_second_tag"][self.sample]
        fc = self.config["params"]["disc_f"]["fc"]

        plot_roc = RocPlot(
            n_ratio_panels=2,
            ylabel="Background rejection",
            xlabel="$b$-jet efficiency",
            atlas_first_tag=atlas_first_tag,
            atlas_second_tag=atlas_second_tag + ", $f_{c}=$" + f"{fc:.3f}",
            figsize=(6, 6),
            y_scale=1.4,
            xmin=lim_eff[0],
            xmax=lim_eff[1],
        )

        for model_name, _dict_rej in dict_rej_models.items():
            dict_rej = _dict_rej["btag"]
            roc = Roc(
                sig_eff=dict_rej["eff_b"][
                    (dict_rej["eff_b"] >= lim_eff[0])
                    & (dict_rej["eff_b"] <= lim_eff[1])
                ],
                bkg_rej=dict_rej["rej_l"][
                    (dict_rej["eff_b"] >= lim_eff[0])
                    & (dict_rej["eff_b"] <= lim_eff[1])
                ],
                rej_class="Light-jets",
                signal_class="bjets",
                label=model_name,
                n_test=n_jets,
            )
            plot_roc.add_roc(
                roc, reference=True if model_name == self.reference_model else False
            )

            roc = Roc(
                sig_eff=dict_rej["eff_b"][
                    (dict_rej["eff_b"] >= lim_eff[0])
                    & (dict_rej["eff_b"] <= lim_eff[1])
                ],
                bkg_rej=dict_rej["rej_c"][
                    (dict_rej["eff_b"] >= lim_eff[0])
                    & (dict_rej["eff_b"] <= lim_eff[1])
                ],
                rej_class="c-jets",
                signal_class="bjets",
                label=model_name,
                n_test=n_jets,
            )
            plot_roc.add_roc(
                roc, reference=True if model_name == self.reference_model else False
            )

        plot_roc.set_ratio_class(1, "Light-jets", "Light-jets ratio")
        plot_roc.set_ratio_class(2, "c-jets", "c-jets ratio")
        plot_roc.draw()
        if self.save_dir:
            plot_roc.savefig(
                os.path.join(self.save_dir, "btag_rejection_comparison.pdf")
            )
            print(
                f"Saved {os.path.join(self.save_dir, 'btag_rejection_comparison.pdf')}"
            )
        else:
            print("No save_dir")

        return plot_roc

    def plot_ctag_rejection_comparison(
        self,
        lim_eff: Tuple[float, float] = (0.6, 1.0),
    ):
        dict_rej_models = self.dict_rej_models
        n_jets = self.n_jets
        atlas_first_tag = self.config["atlas_tag"]["atlas_first_tag"]
        atlas_second_tag = self.config["atlas_tag"]["atlas_second_tag"][self.sample]
        fb = self.config["params"]["disc_f"]["fb"]

        plot_roc = RocPlot(
            n_ratio_panels=2,
            ylabel="Background rejection",
            xlabel="$c$-jet efficiency",
            atlas_first_tag=atlas_first_tag,
            atlas_second_tag=atlas_second_tag + ", $f_{b}=$" + f"{fb:.3f}",
            figsize=(6, 6),
            y_scale=1.4,
            xmin=lim_eff[0],
            xmax=lim_eff[1],
        )

        for model_name, _dict_rej in dict_rej_models.items():
            dict_rej = _dict_rej["ctag"]
            roc = Roc(
                sig_eff=dict_rej["eff_c"][
                    (dict_rej["eff_c"] >= lim_eff[0])
                    & (dict_rej["eff_c"] <= lim_eff[1])
                ],
                bkg_rej=dict_rej["rej_l"][
                    (dict_rej["eff_c"] >= lim_eff[0])
                    & (dict_rej["eff_c"] <= lim_eff[1])
                ],
                rej_class="Light-jets",
                signal_class="cjets",
                label=model_name,
                n_test=n_jets,
            )
            plot_roc.add_roc(
                roc, reference=True if model_name == self.reference_model else False
            )

            roc = Roc(
                sig_eff=dict_rej["eff_c"][
                    (dict_rej["eff_c"] >= lim_eff[0])
                    & (dict_rej["eff_c"] <= lim_eff[1])
                ],
                bkg_rej=dict_rej["rej_b"][
                    (dict_rej["eff_c"] >= lim_eff[0])
                    & (dict_rej["eff_c"] <= lim_eff[1])
                ],
                rej_class="b-jets",
                signal_class="cjets",
                label=model_name,
                n_test=n_jets,
            )
            plot_roc.add_roc(
                roc, reference=True if model_name == self.reference_model else False
            )

        plot_roc.set_ratio_class(1, "Light-jets", "Light-jets ratio")
        plot_roc.set_ratio_class(2, "b-jets", "b-jets ratio")
        plot_roc.draw()
        if self.save_dir:
            plot_roc.savefig(
                os.path.join(self.save_dir, "ctag_rejection_comparison.pdf")
            )
            print(
                f"Saved {os.path.join(self.save_dir, 'ctag_rejection_comparison.pdf')}"
            )
        else:
            print("No save_dir")

        return plot_roc

    def var_vs_plot(
        self,
        var: Literal["pt", "eta", "absEta", "n_tracks"],
        signal_flavor: Literal["b", "c"],
        background_flavor: Literal["l", "c", "b"],
        bins: List[float],
        ylim: Tuple[float, float],
        x_label: Optional[str] = None,
        working_point: Optional[float] = None,
        logy: bool = False,
    ):
        if signal_flavor == "b":
            arr_is_signal = self.df_concat["HadronConeExclTruthLabelID"].values == 5
            atlas_second_tag = (
                self.config["atlas_tag"]["atlas_second_tag"][self.sample]
                + f", $f_c$ = {self.config['params']['disc_f']['fc']:.3f}, b-tagging {working_point*100:.0f}% WP"
            )
        elif signal_flavor == "c":
            arr_is_signal = self.df_concat["HadronConeExclTruthLabelID"].values == 4
            atlas_second_tag = (
                self.config["atlas_tag"]["atlas_second_tag"][self.sample]
                + f", $f_b$ = {self.config['params']['disc_f']['fb']:.3f}, c-tagging {working_point*100:.0f}% WP"
            )
        else:
            raise ValueError(f"Invalid signal flavor: {signal_flavor}")

        if background_flavor == "l":
            arr_is_background = self.df_concat["HadronConeExclTruthLabelID"].values == 0
            ylabel = "Light-flavour jets rejection"
        elif background_flavor == "c":
            arr_is_background = self.df_concat["HadronConeExclTruthLabelID"].values == 4
            ylabel = "c-jets rejection"
        elif background_flavor == "b":
            arr_is_background = self.df_concat["HadronConeExclTruthLabelID"].values == 5
            ylabel = "b-jets rejection"

        if var == "pt":
            xlabel = r"$p_{T}$ [GeV]"
            x_var_sig = self.df_concat["pt"].values[arr_is_signal] / 1000
            x_var_bkg = self.df_concat["pt"].values[arr_is_background] / 1000
        elif var == "eta":
            xlabel = r"$\eta$"
            x_var_sig = self.df_concat["eta"].values[arr_is_signal]
            x_var_bkg = self.df_concat["eta"].values[arr_is_background]
        elif var == "absEta":
            xlabel = r"$|\eta|$"
            x_var_sig = np.abs(self.df_concat["eta"].values[arr_is_signal])
            x_var_bkg = np.abs(self.df_concat["eta"].values[arr_is_background])
        elif var == "n_tracks":
            xlabel = r"$N_{\mathrm{tracks}}$"
            x_var_sig = self.df_concat["n_tracks"].values[arr_is_signal]
            x_var_bkg = self.df_concat["n_tracks"].values[arr_is_background]

        plot_bkg_rej = VarVsEffPlot(
            mode="bkg_rej",
            xlabel=xlabel,
            ylabel=ylabel,
            ymin=ylim[0],
            ymax=ylim[1],
            logy=logy,
            atlas_first_tag=self.config["atlas_tag"]["atlas_first_tag"],
            atlas_second_tag=atlas_second_tag,
            n_ratio_panels=1,
        )

        for model_name in self.model_names:
            var_vs_eff = VarVsEff(
                x_var_sig=x_var_sig,
                disc_sig=self.df_concat[f"D{signal_flavor}_{model_name}"].values[
                    arr_is_signal
                ],
                x_var_bkg=x_var_bkg,
                disc_bkg=self.df_concat[f"D{signal_flavor}_{model_name}"].values[
                    arr_is_background
                ],
                bins=bins,
                working_point=working_point,
                label=model_name,
            )
            plot_bkg_rej.add(
                var_vs_eff,
                reference=True if model_name == self.reference_model else False,
            )

        plot_bkg_rej.draw()
        plot_bkg_rej.savefig(
            os.path.join(
                self.save_dir, f"{var}_{signal_flavor}_{background_flavor}.pdf"
            )
        ) if self.save_dir else None

    def plot_btag_iso_efficiency(
        self,
        fc_values: List[float],
        fc_target_value: float,
        wp: float,
        use_log: bool = False,
        xlim: Optional[Tuple[float, float]] = None,
        ylim: Optional[Tuple[float, float]] = None,
        show_startend: bool = False,
    ):
        def _calc_disc(df_concat, model_name, fc_value):
            return np.log(
                df_concat[f"{model_name}_pb"].values
                / (
                    (1 - fc_value) * df_concat[f"{model_name}_pu"].values
                    + fc_value * df_concat[f"{model_name}_pc"].values
                )
            )

        def _calc_effs(df_concat, model_name, fc_value, wp):
            disc = _calc_disc(df_concat, model_name, fc_value)

            is_b = df_concat["HadronConeExclTruthLabelID"].values == 5
            is_c = df_concat["HadronConeExclTruthLabelID"].values == 4
            is_light = df_concat["HadronConeExclTruthLabelID"].values == 0

            ujets_eff = calc_eff(
                sig_disc=disc[is_b], bkg_disc=disc[is_light], target_eff=wp
            )
            cjets_eff = calc_eff(
                sig_disc=disc[is_b], bkg_disc=disc[is_c], target_eff=wp
            )

            return [fc_value, ujets_eff, cjets_eff]

        frac_plot = Line2DPlot(
            atlas_first_tag=self.config["atlas_tag"]["atlas_first_tag"],
            atlas_second_tag=self.config["atlas_tag"]["atlas_second_tag"][self.sample]
            + f", b-tagging {wp*100:.0f}% WP",
        )

        dict_results = {}

        for model_name in self.model_names:
            eff_results = np.array(
                [
                    _calc_effs(self.df_concat, model_name, fc_value, wp)
                    for fc_value in fc_values
                ]
            )
            x_values = eff_results[:, 2]
            y_values = eff_results[:, 1]
            dict_results[model_name] = {"x_values": x_values, "y_values": y_values}

        for model_name in self.model_names:
            frac_plot.add(
                Line2D(
                    x_values=dict_results[model_name]["x_values"],
                    y_values=dict_results[model_name]["y_values"],
                    label=model_name,
                    linestyle="-",
                )
            )
        print("fc_target_value", fc_target_value)
        print("selected index", np.argmin(np.abs(fc_values - fc_target_value)))
        print(
            "selected fc value",
            fc_values[np.argmin(np.abs(fc_values - fc_target_value))],
        )

        for i, model_name in enumerate(self.model_names):
            frac_plot.add(
                Line2D(
                    x_values=dict_results[model_name]["x_values"][
                        np.argmin(np.abs(fc_values - fc_target_value))
                    ],
                    y_values=dict_results[model_name]["y_values"][
                        np.argmin(np.abs(fc_values - fc_target_value))
                    ],
                    marker="x",
                    markersize=10,
                    colour="black",
                    label=f"$f_c=${fc_target_value:.3f}"
                    if i == len(self.model_names) - 1
                    else None,
                ),
                is_marker=True,
            )
            if show_startend:
                # marker minimum fc
                frac_plot.add(
                    Line2D(
                        x_values=dict_results[model_name]["x_values"][0],
                        y_values=dict_results[model_name]["y_values"][0],
                        marker="<",
                        markersize=5,
                        colour="black",
                        label=f"$f_c=${fc_values[0]:.3f}"
                        if i == len(self.model_names) - 1
                        else None,
                    ),
                    is_marker=True,
                )
                # marker maximum fc
                frac_plot.add(
                    Line2D(
                        x_values=dict_results[model_name]["x_values"][-1],
                        y_values=dict_results[model_name]["y_values"][-1],
                        marker=">",
                        markersize=5,
                        colour="black",
                        label=f"$f_c=${fc_values[-1]:.3f}"
                        if i == len(self.model_names) - 1
                        else None,
                    ),
                    is_marker=True,
                )

        frac_plot.ylabel = "Light-flavour jets efficiency"
        frac_plot.xlabel = "c-jets efficiency"
        frac_plot.logx = use_log
        frac_plot.logy = use_log
        frac_plot.set_xlim(*xlim) if xlim else None
        frac_plot.set_ylim(*ylim) if ylim else None

        frac_plot.draw()
        frac_plot.savefig(
            os.path.join(self.save_dir, f"btag_iso_efficiency.pdf")
        ) if self.save_dir else None

    def plot_ctag_iso_efficiency(
        self,
        fb_values: List[float],
        fb_target_value: float,
        wp: float,
        use_log: bool = False,
        xlim: Optional[Tuple[float, float]] = None,
        ylim: Optional[Tuple[float, float]] = None,
        show_startend: bool = False,
    ):
        def _calc_disc(df_concat, model_name, fb_value):
            return np.log(
                df_concat[f"{model_name}_pc"].values
                / (
                    (1 - fb_value) * df_concat[f"{model_name}_pu"].values
                    + fb_value * df_concat[f"{model_name}_pb"].values
                )
            )

        def _calc_effs(df_concat, model_name, fb_value, wp):
            disc = _calc_disc(df_concat, model_name, fb_value)

            is_b = df_concat["HadronConeExclTruthLabelID"].values == 5
            is_c = df_concat["HadronConeExclTruthLabelID"].values == 4
            is_light = df_concat["HadronConeExclTruthLabelID"].values == 0

            ujets_eff = calc_eff(
                sig_disc=disc[is_c], bkg_disc=disc[is_light], target_eff=wp
            )
            bjets_eff = calc_eff(
                sig_disc=disc[is_c], bkg_disc=disc[is_b], target_eff=wp
            )

            return [fb_value, ujets_eff, bjets_eff]

        frac_plot = Line2DPlot(
            atlas_first_tag=self.config["atlas_tag"]["atlas_first_tag"],
            atlas_second_tag=self.config["atlas_tag"]["atlas_second_tag"][self.sample]
            + f", c-tagging {wp*100:.0f}% WP",
        )

        dict_results = {}

        for model_name in self.model_names:
            eff_results = np.array(
                [
                    _calc_effs(self.df_concat, model_name, fb_value, wp)
                    for fb_value in fb_values
                ]
            )
            x_values = eff_results[:, 2]
            y_values = eff_results[:, 1]
            dict_results[model_name] = {"x_values": x_values, "y_values": y_values}

        for model_name in self.model_names:
            frac_plot.add(
                Line2D(
                    x_values=dict_results[model_name]["x_values"],
                    y_values=dict_results[model_name]["y_values"],
                    label=model_name,
                    linestyle="-",
                )
            )

        print("fb_target_value", fb_target_value)
        print("selected index", np.argmin(np.abs(x_values - fb_target_value)))
        print(
            "selected fb value",
            fb_values[np.argmin(np.abs(fb_values - fb_target_value))],
        )

        for i, model_name in enumerate(self.model_names):
            frac_plot.add(
                Line2D(
                    x_values=dict_results[model_name]["x_values"][
                        np.argmin(np.abs(fb_values - fb_target_value))
                    ],
                    y_values=dict_results[model_name]["y_values"][
                        np.argmin(np.abs(fb_values - fb_target_value))
                    ],
                    marker="x",
                    markersize=10,
                    colour="black",
                    label=f"$f_b=${fb_target_value:.3f}"
                    if i == len(self.model_names) - 1
                    else None,
                ),
                is_marker=True,
            )
            if show_startend:
                # marker minimum fb
                frac_plot.add(
                    Line2D(
                        x_values=dict_results[model_name]["x_values"][0],
                        y_values=dict_results[model_name]["y_values"][0],
                        marker="<",
                        markersize=5,
                        colour="black",
                        label=f"$f_b=${fb_values[0]:.3f}"
                        if i == len(self.model_names) - 1
                        else None,
                    ),
                    is_marker=True,
                )
                # marker maximum fb
                frac_plot.add(
                    Line2D(
                        x_values=dict_results[model_name]["x_values"][-1],
                        y_values=dict_results[model_name]["y_values"][-1],
                        marker=">",
                        markersize=5,
                        colour="black",
                        label=f"$f_b=${fb_values[-1]:.3f}"
                        if i == len(self.model_names) - 1
                        else None,
                    ),
                    is_marker=True,
                )

        frac_plot.logx = use_log
        frac_plot.logy = use_log
        frac_plot.set_xlim(*xlim) if xlim else None
        frac_plot.set_ylim(*ylim) if ylim else None

        frac_plot.ylabel = "Light-flavour jets efficiency"
        frac_plot.xlabel = "b-jets efficiency"

        frac_plot.draw()
        frac_plot.savefig(
            os.path.join(self.save_dir, f"ctag_iso_efficiency.pdf")
        ) if self.save_dir else None


@dataclasses.dataclass
class RocRatios:
    sig_eff: np.ndarray
    bkg_rej: np.ndarray
    err: np.ndarray


class RocAnalyzer:
    def __init__(self, model_names: List[str], plot_roc: RocPlot):
        self.model_names = model_names
        self.plot_roc = plot_roc

    def get_binomial_error(self, roc: Roc) -> np.ndarray:
        return roc.binomial_error()[roc.non_zero_mask]

    def get_roc_values(self) -> Dict[str, RocRatios]:
        roc_values = {}
        dict_bkg_linestyle = self.plot_roc.rej_class_ls
        bkg_names = list(dict_bkg_linestyle.keys())
        dict_mdoels_keys: Dict[
            Literal["Light-jets", "c-jets", "b-jets"], Dict[str, int]
        ] = {}
        for bkg_name in bkg_names:
            dict_mdoels_keys[bkg_name] = {}
        for k, v in self.plot_roc.rocs.items():
            for bkg_name in bkg_names:
                if v.linestyle == dict_bkg_linestyle[bkg_name]:
                    dict_mdoels_keys[bkg_name][v.label] = k
                    break

        for model_name in self.model_names:
            roc_values[model_name] = {}
            for bkg_name in bkg_names:
                roc_values[model_name][bkg_name] = dataclasses.asdict(
                    RocRatios(
                        sig_eff=self.plot_roc.rocs[
                            dict_mdoels_keys[bkg_name][model_name]
                        ].sig_eff,
                        bkg_rej=self.plot_roc.rocs[
                            dict_mdoels_keys[bkg_name][model_name]
                        ].bkg_rej,
                        err=self.get_binomial_error(
                            self.plot_roc.rocs[dict_mdoels_keys[bkg_name][model_name]]
                        ),
                    )
                )
        return roc_values

    def get_rejection_vlues_at_wp(self, wp: float) -> Dict[str, Dict[str, List[float]]]:
        dict_roc_values = self.get_roc_values()
        dict_rejection_values = {}
        for model_name in self.model_names:
            dict_rejection_values[model_name] = {}
            for bkg_name in dict_roc_values[model_name].keys():
                value = dict_roc_values[model_name][bkg_name]["bkg_rej"][
                    np.argmin(
                        np.abs(dict_roc_values[model_name][bkg_name]["sig_eff"] - wp)
                    )
                ]
                err = dict_roc_values[model_name][bkg_name]["err"][
                    np.argmin(
                        np.abs(dict_roc_values[model_name][bkg_name]["sig_eff"] - wp)
                    )
                ]
                dict_rejection_values[model_name][bkg_name] = [float(value), float(err)]
        return dict_rejection_values
