import os
from typing import Tuple, Dict, List, Union, Any, Optional, Literal
import argparse
import random
import warnings

warnings.simplefilter('ignore')

import h5py
import yaml
import json
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from dotenv import load_dotenv
from tqdm import tqdm

from comet_ml import Experiment, ExistingExperiment
from comet_ml.integration.pytorch import log_model

import torch
from torch.utils.data import Dataset, TensorDataset, DataLoader
from weaver.nn.model import ParticleNet

from modules import data, eval, exec, fix_seed, file_util, MyParT

warnings.simplefilter("default")

def get_lorentz_idx(var_trks_use:List[str]):
    return {
        "pt": var_trks_use.index("pt"),
        "deta": var_trks_use.index("deta"),
        "dphi": var_trks_use.index("dphi"),
    }

def read_scale_dict(data_path:str):
    json_path = os.path.join(data_path, "preprocessed", "scale_dict.json")
    with open(json_path, "r") as f:
        scale_dict = json.load(f)
    return scale_dict

def test_wrapper(
    config,
    dir_path:str,
    save_path:str,
    target_epoch:int,
    ):
    is_multi_task:bool = config["is_multi_task"] if "is_multi_task" in config.keys() else False
    print(f"is_multi_task: {is_multi_task}")
    # make model
    if is_multi_task:
        model = MyParT.MyParTTaggerMultiTask(
            **config["hyperparameters"]["model"],
        ).to(device)
    else:
        model = MyParT.MyParTTagger(
            **config["hyperparameters"]["model"],
        ).to(device)

    # optimizer, scheduler, loss function
    if config["hyperparameters"]["optimizer"]["name"] == "adamw":
        optimizer = torch.optim.AdamW(
            model.parameters(),
            lr=config["hyperparameters"]["optimizer"]["lr_init"],
            weight_decay=config["hyperparameters"]["optimizer"]["weight_decay"],
        )
    elif config["hyperparameters"]["optimizer"]["name"] == "ranger":
        from weaver.utils.nn.optimizer import ranger # LookAhead + RAdam
        optimizer = ranger.Ranger(
            model.parameters(),
            **config["hyperparameters"]["optimizer"]["params"] if "params" in config["hyperparameters"]["optimizer"].keys() else {},
        )
    else:
        raise ValueError("optimizer must be adamw or ranger")

    loss_fn = torch.nn.CrossEntropyLoss(reduction="mean")

    # load model
    model.load_state_dict(torch.load(os.path.join(dir_path, f"{target_epoch:03d}", "model.pth")))

    list_use_dataset_name = ["ttbar", "zprime"] if parser_sample == "all" else [parser_sample]
    dict_dataset_name_tex = {
        "ttbar": "$t\\bar{t}$",
        "zprime": "$Z'$",
    }

    for dataset_name in list_use_dataset_name:
        print(f"start testing {dataset_name}")
        dataset_name_tex = dict_dataset_name_tex[dataset_name]

        test_dataset = data.TestDataset2(
            # data_path = config["dataset"]["data_path"],
            data_path = "/home/suzukiy/ftag/umami-dataset/umami-020-GNx", # datasetをあわせるため
            dataset_name = dataset_name,
            var_trks_use = config["dataset"]["var_trks_use"],
            scale_dict = read_scale_dict(config["dataset"]["data_path"]),
            batch_size = config["hyperparameters"]["batch_size"],
            size = None,
            is_multi_task = is_multi_task,
        )

        test_loader, df_meta = test_dataset.create_dataloader_metadata()

        y_test = df_meta[test_dataset.flavour_variable_name].values
        y_test = np.stack([y_test == 0, y_test == 4, y_test == 5], axis=1)

        save_figure_basepath = os.path.join(save_path, "figure", dataset_name)
        if not os.path.exists(save_figure_basepath):
            os.makedirs(save_figure_basepath)
            print(f"make dir: {save_figure_basepath}")
        
        if is_multi_task:
            y_pred, concat_dict_task_losses = exec.test_parT_multi_task(
                model=model,
                dataloader=test_loader,
                loss_fn=loss_fn,
                device=device,
                lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                edge_config=config["dataset"]["edge_config"],
                loss_weights=config["multi_task"]["loss_weights"],
                multi_task_save_h5_path=os.path.join(save_path, "figure", dataset_name, "multi_task.h5"),
                scale_dict = read_scale_dict(config["dataset"]["data_path"]),
            )
        else:
            y_pred, _ = exec.test_parT(
                model=model,
                dataloader=test_loader,
                loss_fn=loss_fn,
                device=device,
                lorentz_idx=get_lorentz_idx(config["dataset"]["var_trks_use"]),
                edge_config=config["dataset"]["edge_config"],
                scale_dict = read_scale_dict(config["dataset"]["data_path"]),
            )

        
        df_pred = pd.DataFrame(y_pred, columns=["pred_l", "pred_c", "pred_b"])
        df_test = pd.DataFrame(y_test, columns=["true_l", "true_c", "true_b"])
        df_concat = pd.concat([df_pred, df_test, df_meta], axis=1)
        print(f"{len(df_concat)} events will be saved in {os.path.join(save_path, 'figure', dataset_name, 'test_y_pred_y_test.csv')}")
        df_concat.to_csv(os.path.join(save_path, "figure", dataset_name, "test_y_pred_y_test.csv"), index=False)

        # remove events neither b nor c nor l
        y_pred = y_pred[y_test.sum(axis=1) == 1]
        y_test = y_test[y_test.sum(axis=1) == 1]

        print(f"y_pred.shape: {y_pred.shape}")
        print(f"y_test.shape: {y_test.shape}")

        try:
            eval.plot_prob_btag(
                y_pred, y_test,
                dataset_name_tex,
                save_path=save_figure_basepath,
            )
            
            eval.plot_prob_ctag(
                y_pred, y_test,
                dataset_name_tex,
                save_path=save_figure_basepath,
            )
            
            eval.bctag_evaluation(
                y_pred, y_test, dataset_name_tex,
                fc=config["metrics"]["fc"],
                fb=config["metrics"]["fb"],
                save_path=save_figure_basepath,
            )
        except ValueError as e:
            print(e)
            print("skip plotting")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--dir_path", type=str)
    parser.add_argument("-s", "--sample", choices=["ttbar", "zprime", "all"], type=str, default="all")
    parser.add_argument("-m", "--mode", choices=["best", "all", "specific", "acc", "loss"], default="acc")
    parser.add_argument("-e", "--epoch", type=int, default=-1)
    parser.add_argument("--limit", type=int, default=-1)

    args = parser.parse_args()
    dir_path:str = args.dir_path
    parser_sample:str = args.sample
    parser_mode:str = args.mode
    parser_epoch:int = args.epoch
    parser_limit:int = args.limit

    with open(os.path.join(dir_path, "config.yaml"), "r") as f:
        config = yaml.safe_load(f)
    
    
    experiment = ExistingExperiment(
        project_name="part-ftag",
        experiment_key=config["experiment_key"],
    )

    # seed settings
    seed = config["seed"]
    fix_seed.fix_seed(seed)

    # device
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print(device)

    with experiment.test():
        if parser_mode == "best" or parser_mode == "acc" or parser_mode == "loss":
            # read history
            history = pd.read_csv(os.path.join(dir_path, "history.csv"))
            if parser_limit != -1:
                history = history[:parser_limit]
                print(f"limit history to {parser_limit}")
            best_val_acc_epoch = history["val_acc"].idxmax()
            best_val_loss_epoch = history["val_loss"].idxmin()
            best_btag_rej_c_epoch = history["val_btag_rej_c"].idxmax()
            best_btag_rej_l_epoch = history["val_btag_rej_l"].idxmax()
            best_ctag_rej_b_epoch = history["val_ctag_rej_b"].idxmax()
            best_ctag_rej_l_epoch = history["val_ctag_rej_l"].idxmax()

            if parser_mode == "acc":
                dict_best_epochs = {"best_val_acc_epoch": best_val_acc_epoch}
            elif parser_mode == "loss":
                dict_best_epochs = {"best_val_loss_epoch": best_val_loss_epoch}
            else:
                dict_best_epochs = {
                    "best_val_acc_epoch": best_val_acc_epoch,
                    "best_val_loss_epoch": best_val_loss_epoch,
                    "best_btag_rej_c_epoch": best_btag_rej_c_epoch,
                    "best_btag_rej_l_epoch": best_btag_rej_l_epoch,
                    "best_ctag_rej_b_epoch": best_ctag_rej_b_epoch,
                    "best_ctag_rej_l_epoch": best_ctag_rej_l_epoch,
                }

            for k, target_epoch in dict_best_epochs.items():
                print(f"{k}: {target_epoch}")
                save_path = os.path.join(dir_path, f"{k}_{target_epoch:03d}")
                if not os.path.exists(save_path):
                    os.makedirs(save_path)
                    print(f"make dir: {save_path}")
                    # {v:03d}ディレクトリの中のファイルをコピー
                    os.system(f"cp -r {os.path.join(dir_path, f'{target_epoch:03d}', '*')} {save_path}")
                    print(f"copy files from {os.path.join(dir_path, f'{target_epoch:03d}', '*')} to {save_path}")
                
                test_wrapper(config, dir_path, save_path, target_epoch)
        
        elif parser_mode == "specific":
            if parser_epoch == -1:
                raise ValueError("epoch is not specified")
            else:
                print(f"target epoch: {parser_epoch}")
                save_path = os.path.join(dir_path, f"{parser_epoch:03d}")
                test_wrapper(config, dir_path, save_path, target_epoch=parser_epoch)
        
        elif parser_mode == "all":
            print("start testing all epochs: it may take a long time")
            for target_epoch in range(config["hyperparameters"]["epochs"]):
                print(f"target epoch: {target_epoch}")
                save_path = os.path.join(dir_path, f"{target_epoch:03d}")                
                test_wrapper(config, dir_path, save_path, target_epoch)            
    
    print("finish testing")